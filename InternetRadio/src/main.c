/*! \mainpage SIR firmware documentation
*
*  \section intro Introduction
*  A collection of HTML-files has been generated using the documentation in the sourcefiles to
*  allow the developer to browse through the technical documentation of this project.
*  \par
*  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
*  documentation should be done via the sourcefiles.
*/

/*! \file
*  COPYRIGHT (C) STREAMIT BV 2010
*  \date 19 december 2003
*/



#define PAGE_NUMBER 60
#define PAGE_NUMBER_SNOOZE 85
#define LOG_MODULE  LOG_MAIN_MODULE
//#define USE_JTAG 1

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <dev/board.h>
#include <io.h>

#include <arpa/inet.h> /* [LiHa] For inet_addr() */


#include "hardware/system.h"
#include "hardware/portio.h"
#include "hardware/display.h"
#include "hardware/remcon.h"
#include "hardware/keyboard.h"
#include "hardware/led.h"
#include "hardware/log.h"
#include "control/uart0driver.h"
#include "hardware/mmc.h"
#include "control/watchdog.h"
#include "hardware/flash.h"
#include "hardware/spidrv.h"
#include "application/menu.h"
#include <time.h>
#include "hardware/rtc.h"
#include "util/customChars.h"
#include "control/flash_drive.h"
#include "control/radio_display.h"
#include "control/radio_settings.h"
#include "control/radio_alarm.h"
#include "control/shoutcast.h"
#include "control/ethernet.h"
#include "util/menuStrings.h"
#include "hardware/vs10xx.h"
#include "control/ethernet.h"
#include "control/radio_week_alarm.h"
//#include "application/radio_alarm.h"

void ScanKeys(void);
void ShutDown(void);
void SetTime(void);

#define NO_MAC -1
#define HAS_MAC 0




THREAD (WifiSignal, arg);
THREAD (AnimationThread, arg);
THREAD (AlarmSoundTest, arg);


/*-------------------------------------------------------------------------*/
/* global variable definitions                                             */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void SysMainBeatInterrupt(void*);
static void SysControlMainBeat(u_char);

int released;
time_t startTime;
time_t currentTime;
time_t timerDuration;
time_t endTime;
int idx = 1;
int tunes[50];

/*-------------------------------------------------------------------------*/
/* Stack check variables placed in .noinit section                         */
/*-------------------------------------------------------------------------*/

/*!
* \addtogroup System
*/

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/


/* ����������������������������������������������������������������������� */
/*!
* \brief ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
*
* This routine is automatically called during system
* initialization.
*
* resolution of this Timer ISR is 4,448 msecs
*
* \param *p not used (might be used to pass parms from the ISR)
*/
/* ����������������������������������������������������������������������� */
static void SysMainBeatInterrupt(void *p)
{

	/*
	*  scan for valid keys AND check if a MMCard is inserted or removed
	*/
	KbScan();
	CardCheckCard();
}


/* ����������������������������������������������������������������������� */
/*!
* \brief Initialise Digital IO
*  init inputs to '0', outputs to '1' (DDRxn='0' or '1')
*
*  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
*  is written to the pin (PORTxn='1')
*/
/* ����������������������������������������������������������������������� */
void SysInitIO(void)
{
	/*
	*  Port B:     VS1011, MMC CS/WP, SPI
	*  output:     all, except b3 (SPI Master In)
	*  input:      SPI Master In
	*  pull-up:    none
	*/
	outp(0xF7, DDRB);

	/*
	*  Port C:     Address bus
	*/

	/*
	*  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
	*  output:     Keyboard colums 2 & 3
	*  input:      LCD_data, SDA, SCL (TWI)
	*  pull-up:    LCD_data, SDA & SCL
	*/
	outp(0x0C, DDRD);
	outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

	/*
	*  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
	*  output:     CS Flash, LCD BL/Enable, USB Tx
	*  input:      VS1011 (DREQ), RTL8019, IR
	*  pull-up:    USB Rx
	*/
	outp(0x8E, DDRE);
	outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

	/*
	*  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
	*  output:     LCD RS/RW, LED
	*  input:      Keyboard_Rows, MCC-detect
	*  pull-up:    Keyboard_Rows, MCC-detect
	*  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
	*/
	#ifndef USE_JTAG
	sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
	sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256

	
	#endif //USE_JTAG

	cbi(OCDR, IDRD);
	cbi(OCDR, IDRD);


	outp(0x0E, DDRF);
	outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

	/*
	*  Port G:     Keyboard_cols, Bus_control
	*  output:     Keyboard_cols
	*  input:      Bus Control (internal control)
	*  pull-up:    none
	*/
	outp(0x18, DDRG);
}

/* ����������������������������������������������������������������������� */
/*!
* \brief Starts or stops the 4.44 msec mainbeat of the system
* \param OnOff indicates if the mainbeat needs to start or to stop
*/
/* ����������������������������������������������������������������������� */
static void SysControlMainBeat(u_char OnOff)
{
	int nError = 0;

	if (OnOff==ON)
	{
		nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
		if (nError == 0)
		{
			init_8_bit_timer();
		}
	}
	else
	{
		// disable overflow interrupt
		disable_8_bit_timer_ovfl_int();
	}
}

/* ����������������������������������������������������������������������� */
/*!
* \brief Main entry of the SIR firmware
*
* All the initialisations before entering the for(;;) loop are done BEFORE
* the first key is ever pressed. So when entering the Setup (POWER + VOLMIN) some
* initialisatons need to be done again when leaving the Setup because new values
* might be current now
*
* \return \b never returns
*/
/* ����������������������������������������������������������������������� */
int main(void)
{
	WatchDogDisable();

	NutDelay(100);

	SysInitIO();
	SPIinit();
	LedInit();
	Display_Init();
	Uart0DriverInit();
	
	Uart0DriverStart();
	LogInit();
	LogMsg_P(LOG_INFO, PSTR("Hello World"));

	CardInit();
	
	RadioSettings_Init();
	
	
	if (At45dbInit()==AT45DB041B)
	{
		// ......
	}


	RcInit();
	
	KbInit();
	
	radio_alarmInit();
	
	SysControlMainBeat(ON); // enable 4.4 msecs hartbeat interrupt
	
	/*
	* Increase our priority so we can feed the watchdog.
	*/
	NutThreadSetPriority(1);

	/* Enable global interrupts */
	sei();
	
	
	LcdBackLight(LCD_BACKLIGHT_OFF);
	released = 1;
	
	//Starts up the menu
	unsigned char *bufMac = NULL;
	bufMac = (unsigned char *) malloc(1 + sizeof(unsigned char) * 12);
	At45dbPageRead(PAGE_NUMBER, bufMac, sizeof(unsigned char) * 12);
	printf("check bufmac: %s", bufMac);
	
	
	unsigned char *bufSnooze = NULL;
	bufSnooze = (unsigned char *) malloc (1+ sizeof(unsigned char) * 2);
	At45dbPageRead(PAGE_NUMBER_SNOOZE, bufSnooze, sizeof(unsigned char) * 2);
	printf("check bufsnooze: %s", bufSnooze);
	
	//tm temptime;

	//printf("NTP time is: %02d:%02d:%02d\n", temptime.tm_hour, temptime.tm_min, temptime.tm_sec);

	if ((bufMac != NULL) && (bufMac[0] == '\0')) {
		StartMenuStartUp(NO_MAC, bufMac, bufSnooze);
	}
	else{
		StartMenuStartUp(HAS_MAC, bufMac, bufSnooze);
	}
	free(bufMac);
	
	printf("function executed\n");
	//radio_week_alarm_SaveWeekAlarms();
	radio_week_alarm_LoadWeekAlarms();
	
	NutThreadCreate("AlarmSoundThread", AlarmSoundTest, NULL, 512);
	/*Alarm_ResetToDefault();*/
	if (Alarm_LoadAlarm() == -1)
	{
		printf("Failed reading alarms from flash drive, setting default ones.\n");
	}

	if( OK != Ethernet_Init())
	{
		LogMsg_P(LOG_ERR, PSTR("initInet() = NOK, NO network!"));
		
	}

	

	//NutThreadCreate("Wifisignal", WifiSignal, NULL,512);
	
	for (;;)
	{
		
		NutSleep(1);

		KbScan();

		Menu_Update();
		Alarm_CheckAlarm();
		radio_week_alarm_CheckAlarm();
		
		ScanKeys();
		
		WatchDogRestart();
		
		if (Animationbuf == 1){
			NutThreadCreate("animations", AnimationThread, NULL, 512);
		}


	}
		return(0);      // never reached, but 'main()' returns a non-void, so.....

}

/*
*	Checks if a button is being pressed.
*	When a button is pressed it gives the key to the handleMenu(key).
*/
void ScanKeys()
{
	u_char key = KbGetKey();
	
	if(key != 136 && released == 1)
	{
		u_char lcd = LcdIsOn();
		
		startTime = time(NULL);
		timerDuration = 10;  // 10 second timer
		endTime = startTime + timerDuration;
		LcdBackLight(LCD_BACKLIGHT_ON);
		
		if(lcd == 1)
		HandleMenu(key);
		
		if(key == KEY_POWER)
		{
			UIHandleReset();
		}
		released = 2;
	}
	else if(key == 136)
	{
		
		released = 1;
	}
	time_t currTime = time(NULL);
	time_t timeLeft = endTime - currTime;

	if(timeLeft < 0){
		FlashDrive_ReadFromFlashDrive(NOTES_FLASH_ID, tunes, sizeof(tunes));
		LcdBackLight(LCD_BACKLIGHT_OFF);
	}
	
}

int UIHandleReset()
{
	WatchDogEnable();
	NutSleep(100);
	WatchDogStart(30);
	for (;;) {};
}
/*
*	shutsdown the system when the off button is pressed.
*/
void ShutDown()
{
	int shutDownReleased= 0;
	LcdBackLight(LCD_BACKLIGHT_OFF);
	LcdClearDisplay();
	for(;;)
	{
		KbScan();
		u_char key = KbGetKey();

		if(key == 13 && shutDownReleased == 1)
		{
			//LcdBackLight(LCD_BACKLIGHT_ON);
			break;
		}
		else if(key == 136)
		shutDownReleased = 1;
		
		NutSleep(10);
	}

}

/*
*	Gets the time and sets it on the first line of the screen.
*/
void SetTime(void)
{
	tm gmt;
	
	Display_Clear();
	RadioSettings_GetDateTime(&gmt);
	
	char buffer[5];

	sprintf(buffer, "%02d:%02d", gmt.tm_hour, gmt.tm_min);

	Display_WriteFirstLine(buffer);
}

//set 15th char to cross char.
void ChangeWifiChar(){
	DateString[15]= 0x07;
	MainMenuString[15] = 0x07;
	AlarmString[15] = 0x07;
	AlarmDetailString[15] = 0x07;
	SettingsWifiString[15] = 0x07;
	VolumeWifiString[15] = 0x07;
	SnoozeWifiString[15] = 0x07;
	MacWifiString[15] = 0x07;
	LanguageWifiString[15] = 0x07;
	LanguageNlWifiString[15] = 0x07;
	ChangeTimeWifiString[15] = 0x07;
	SelectHoursString[15] = 0x07;
	SelectMinsString[15] = 0x07;
	RadioMenuString[15] = 0x07;
	RadioChannel1WifiString[15] = 0x07;
	RadioChannel2WifiString[15] = 0x07;

}
//set 15th char to wifi char.
void ChangeWifiCharBack(){
	DateString[15]= 0x06;
	MainMenuString[15] = 0x06;
	AlarmString[15] = 0x06;
	AlarmDetailString[15] = 0x06;
	SettingsWifiString[15] = 0x06;
	VolumeWifiString[15] = 0x06;
	SnoozeWifiString[15] = 0x06;
	MacWifiString[15] = 0x06;
	LanguageWifiString[15] = 0x06;
	LanguageNlWifiString[15] = 0x06;
	ChangeTimeWifiString[15] = 0x06;
	SelectHoursString[15] = 0x06;
	SelectMinsString[15] = 0x06;
	RadioMenuString[15] = 0x06;
	RadioChannel1WifiString[15] = 0x06;
	RadioChannel2WifiString[15] = 0x06;

}



/*
*	Thread to test the wifi connection.
*	Calls Shoutcast_ConnectionCheck and changes the Wifi char if there is no connection.
*	If there is a Wifi connection the char is set back to the wifi char.
*/

THREAD (WifiSignal, arg)
{
	for(;;){
		if (Shoutcast_ConnectionCheck()== -1){
			ChangeWifiChar();
			LogMsg_P(LOG_ERR, PSTR("geen internet"));
		}
		else{
			ChangeWifiCharBack();
		}
		
		NutSleep(100);
	}
	
}
//plays the standard alarm sound.
THREAD (AlarmSoundTest, arg) {
	VsPlayerInit();
	while(1){
		if(radio_alarmAlarmOn == 1){
			if(tunes[0] == NULL){
				printf("alarm should make sound\n");
				VsBeep(10, 2000);
				NutDelay(100);
				VsBeep(10, 20);
				NutDelay(100);
				VsBeep(10, 200);
				NutDelay(100);
				VsBeep(10, 2000);
				NutDelay(500);
			}
			else{
				VsBeep(tunes[idx],2000);
				NutSleep(100);

				idx ++;
				if(idx == tunes[0]){
					idx = 1;
				}
			}
		}
			NutSleep(500);
		
	}
}
/* ---------- end of module ------------------------------------------------ */

// Creates a Thread for the animation that plays when the radio is on.
THREAD (AnimationThread, arg){
	int e = 1;
	while (1) {		LcdClearDisplay();
		switch (e)
		{
			case 1:
			AnimationChar1[7]= 0x01;
			Display_WriteFirstLine(AnimationChar1);
			AnimationChar2[3] = 0x03;
			AnimationChar2[11]= 0x03;
			Display_WriteSecondLine(AnimationChar2);
			e = 2;
			NutDelay(500);
			break;
			case 2:
			AnimationChar1[7]= 0x02;
			Display_WriteFirstLine(AnimationChar1);
			AnimationChar2[3] = 0x02;
			AnimationChar2[11]= 0x02;
			Display_WriteSecondLine(AnimationChar2);
			e = 3;
			NutDelay(500);
			break;
			case 3:
			AnimationChar1[7]= 0x03;
			Display_WriteFirstLine(AnimationChar1);
			AnimationChar2[3] = 0x03;
			AnimationChar2[11]= 0x03;
			Display_WriteSecondLine(AnimationChar2);
			e = 4;
			NutDelay(500);
			break;
			case 4:
			AnimationChar1[7]= 0x04;
			Display_WriteFirstLine(AnimationChar1);
			AnimationChar2[3] = 0x04;
			AnimationChar2[11]= 0x04;
			Display_WriteSecondLine(AnimationChar2);
			e = 1;
			NutDelay(500);
			break;
		}
		NutSleep(500);
		if (Animationbuf == 2){
			NutThreadExit();
			Animationbuf = 0;
		}
	}}


