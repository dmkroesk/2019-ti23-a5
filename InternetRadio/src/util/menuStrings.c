/*
 * menuStrings.c
 *
 * Created: 22-2-2019 10:26:51
 *  Author: A5
 */ 
#include "util/menuStrings.h"
//Date time & Welkom strings
char DateString[17] =			{' ',' ','D','I',' ','1','4','-','0','2','-','1','9',' ',' ',0x06,'\0'};
char WelcomeString[17] =		{' ',' ',' ',' ',' ','W','E','L','K','O','M',' ',' ',' ' ,' ',' ','\0'};
char ClickButtonString[17] =	{' ',' ',' ','D','R','U','K',' ','O','P',' ','O','k',' ',' ',' ', '\0'};
char EnterCodeString[17] =		{'V','o','e','r',' ','c','o','d','e',' ','i','n',' ',' ' ,' ',' ','\0'};

	
//Main Menu items
char MainMenuString[17] =		{' ', ' ',' ','H','O', 'O','F','D', ' ', 'M', 'E', 'N','U',' ', ' ',0x06, '\0'};
char AlarmArrowString[17] =		{' ', '>',' ',' ','A', 'L','A','R', 'M', 'E', 'N', ' ',' ',' ', ' ',' ', '\0'};
char AlarmString[17] =			{' ', ' ',' ',' ','A', 'L','A','R', 'M', 'E', 'N', ' ',' ',' ', ' ',0x06, '\0'};
char SettingsArrowString[17] =	{' ', '>','I','N','S', 'T','E','L', 'L', 'I', 'N', 'G','E','N', ' ',' ', '\0'};
char RadioArrowString[17] =		{' ', '>',' ',' ',' ', 'R','A','D', 'I', 'O', ' ', ' ',' ',' ', ' ',' ', '\0'};
char RadioWifiString[17] =		{' ', ' ',' ',' ',' ', 'R','A','D', 'I', 'O', ' ', ' ',' ',' ', ' ',0x06, '\0'};
char WeekAlarmArrowString[17] = {' ', '>',' ','W','E', 'E','K',' ', 'A', 'L', 'A', 'R','M',' ', ' ',' ', '\0'};
char WeekAlarmWifiString[17] =	{' ', ' ',' ','W','E', 'E','K',' ', 'A', 'L', 'A', 'R','M',' ', ' ',0x06, '\0'};
	
	
//Alarm items
char AlarmListString[17] =		{'>', 'A','L','A','R', 'M','0','1', ' ', '1', '1', ':','0','0', 'A',' ', '\0'};
char AlarmDetailString[17] =	{' ', 'A','L','A','R', 'M','0','1', ' ', '1', '1', ':','0','0', ' ',0x06, '\0'};
char AlarmDetail2String[17] =	{' ', 'O','N',' ',' ', ' ',' ','M', 'D', 'W', 'D', 'V',' ',' ', ' ',' ', '\0'};
		
//Settings items
char SettingsWifiString[17] =	{' ', ' ','I','N','S', 'T','E','L', 'L', 'I', 'N', 'G','E','N', ' ',0x06, '\0'};
char VolumeArrowString[17] =	{' ', '>',' ',' ',' ', 'V','O','L', 'U', 'M', 'E', ' ',' ',' ', ' ',' ', '\0'};
char VolumeWifiString[17] =		{' ', ' ',' ',' ',' ', 'V','O','L', 'U', 'M', 'E', ' ',' ',' ', ' ',0x06, '\0'};
char SnoozeArrowString[17] =	{' ', '>',' ',' ',' ', 'S','N','O', 'O', 'Z', 'E', ' ',' ',' ', ' ',' ', '\0'};
char SnoozeWifiString[17] =		{' ', ' ',' ',' ',' ', 'S','N','O', 'O', 'Z', 'E', ' ',' ',' ', ' ',0x06, '\0'};
char MacArrowString[17] =		{' ', '>',' ','M','A', 'C',' ','A', 'D', 'R', 'E', 'S',' ',' ', ' ',' ', '\0'};
char MacWifiString[17] =		{' ', ' ',' ','M','A', 'C',' ','A', 'D', 'R', 'E', 'S',' ',' ', ' ',0x06, '\0'};
char LanguageArrowString[17] =	{' ', '>',' ',' ',' ', ' ','T','A', 'A', 'L', ' ', ' ',' ',' ', ' ',' ', '\0'};
char SnoozeTimeString[17] =		{' ', ' ',' ',' ',' ', ' ','3','M', 'I', 'N', ' ', ' ',' ',' ', ' ',' ', '\0'};
char LanguageWifiString[17] =	{' ', ' ',' ',' ',' ', ' ','T','A', 'A', 'L', ' ', ' ',' ',' ', ' ',0x06, '\0'};
char LanguageNlArrowString[17] ={' ', '>',' ','N','E', 'D','E','R', 'L', 'A', 'N', 'D','S',' ', ' ',' ', '\0'};
char LanguageNlWifiString[17] = {' ', ' ',' ','N','E', 'D','E','R', 'L', 'A', 'N', 'D','S',' ', ' ',0x06, '\0'};
char LanguageEnString[17] =		{' ', '>',' ',' ',' ', 'E','N','G', 'E', 'L', 'S', ' ',' ',' ', ' ',' ', '\0'};
char ChangeTimeSettingsString[17] = {' ', '>','V','E', 'R','A','N', 'D', 'E', 'R', ' ','T','I', 'J','D',' ','\0'};
char ChangeTimeWifiString[17] = {' ','V','E', 'R','A','N', 'D', 'E', 'R', ' ','T','I', 'J','D',' ',0x06 ,'\0'};
char ChangeNpmAutoString[17] =  {'>',' ',' ', ' ','N','T', 'P', ' ', 'S', 'Y','N','C', ' ',' ',' ',' ' ,'\0'};
char ChangeNpmAutoWifiString[17] =  {' ',' ',' ', ' ','N','T', 'P', ' ', 'S', 'Y','N','C', ' ',' ',' ',0x06 ,'\0'};
	
//Time items
char SelectHoursString[17] =	{' ',' ',' ', ' ',' ','^', '^', ' ', ' ', ' ',' ',' ', ' ',' ',' ', 0x06, '\0'};
char SelectMinsString[17] =		{' ',' ',' ', ' ',' ',' ', ' ', '^', '^', ' ',' ',' ', ' ',' ',' ', 0x06, '\0'};
																					
																					
//Radio Items
char RadioMenuString[17] =		{' ', 'R','A','D','I', 'O',' ','Z', 'E', 'N', 'D', 'E','R','S', ' ',0x06, '\0'};
char RadioChannel1ArrowString[17] =		{' ', '>',' ',' ',' ', ' ','J','A', 'Z', 'Z', ' ', ' ',' ',' ', ' ',' ', '\0'};
char RadioChannel1WifiString[17] =		{' ', ' ',' ',' ',' ', ' ','J','A', 'Z', 'Z', ' ', ' ',' ',' ', ' ',0x06, '\0'};
char RadioChannel2ArrowString[17] =		{' ', '>','C','L','A', 'S','S','I', 'C', ' ', 'R', 'O','C','K', ' ',' ', '\0'};
char RadioChannel2WifiString[17] =		{' ', ' ','C','L','A', 'S','S','I', 'C', ' ', 'R', 'O','C','K', ' ',0x06, '\0'};
char RadioChannel3ArrowString[17] =		{' ', '>',' ',' ','A', 'A','R','D', 'S', 'C', 'H', 'O','K',' ', ' ',' ', '\0'};
char RadioChannel3WifiString[17] =		{' ', ' ',' ',' ','A', 'A','R','D', 'S', 'C', 'H', 'O','K',' ', ' ',0x06, '\0'};
char RadioOffArrowString[17] =			{' ', '>',' ',' ','R', 'A','D','I', 'O', ' ', 'U', 'I','T',' ', ' ',' ', '\0'};
	
//animation
char AnimationChar1[17] =			{' ', ' ',' ',' ',' ', ' ',' ',' ', ' ', ' ', ' ', ' ',' ',' ', ' ',' ', '\0'};
char AnimationChar2[17] =			{' ', ' ',' ',' ',' ', ' ',' ',' ', ' ', ' ', ' ', ' ',' ',' ', ' ',' ', '\0'};

	
	