#define LOG_MODULE  LOG_MAIN_MODULE
#define F_CPU 8000000
#include <stdio.h>
#include <stdlib.h>

#include <util/delay.h>
#include <sys/thread.h>

#include <string.h>
#include <stddef.h>

#include <time.h>

//#include <termios.h> /* For console input on Linux */
//#include <unistd.h>
//#include <windows.h> /* For console input on Windows */
#include "application/menu.h"
#include "util/customChars.h"
#include "hardware/display.h"
#include "control/radio_settings.h"
#include "control/radio_display.h"
#include "hardware/log.h"
#include "hardware/flash.h"
#include "control/shoutcast.h"
#include "control/player.h"
#include "control/flash_drive.h"
#include "application/synthesizer.h"
//#include "global.h"
#include "control/radio_week_alarm.h"
#include "util/menuStrings.h"
#include "control/radio_alarm.h"
#include "control/apidata.h"
#include "control/ethernet.h"
#include "control/radio_week_alarm.h"
#include "application/lockscreen.h"
/*
* menu.c
*
* Created: 19-2-2019 11:49:01
*  Author: A5
*/

int MaxDaysMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
int MaxDaysMonthLeapYear[12] = {31,29,31,30,31,30,31,31,30,31,30,31};
char MacChars[17] = {'0','1','2','3','4','5','6','7','8','9','A','B','D','D','E','F'};
char MAC[12] = {0,0,0,0,0,0,0,0,0,0,0,0};
char SnoozeChars[5] = {'1', '2', '3', '4', '5'};
char SNOOZE[1]= {0};
unsigned char *bufMac = NULL;
unsigned char *bufSnooze = NULL;

#define ALARM_ON 1
#define MAX_HOUR_VALUE 23
#define MAX_MINUTE_VALUE 59

#define MIN_WEEKALARM_INDEX 0
#define MAX_WEEKALARM_INDEX 2

#define MAX_MENU_KEY 6
#define LCD_MAX_LINES 2
#define PAGE_NUMBER 60
#define PAGE_NUMBER_SNOOZE 85
#define MAX_ALARM_TIME 2



/* Defining the keys for the menus*/
#define MENU_KEY_ESC 0
#define MENU_KEY_UP 2
#define MENU_KEY_OK 1
#define MENU_KEY_LEFT 4
#define MENU_KEY_DOWN 3
#define MENU_KEY_RIGHT 5

/* Defining the keys for when they are pressed */
#define CLICK_KEY_1 1
#define CLICK_KEY_2 2
#define CLICK_KEY_3 3
#define CLICK_KEY_4 4
#define CLICK_KEY_5 5
#define CLICK_KEY_6 6
#define CLICK_KEY_ESC 7
#define CLICK_KEY_UP 8
#define CLICK_KEY_OK 9
#define CLICK_KEY_LEFT 10
#define CLICK_KEY_DOWN 11
#define CLICK_KEY_RIGHT 12

/* Defining the screens */
#define SAME_SCREEN -1
#define WELCOME_MENU_ID 1
#define WELCOME_MENU_FIRST_ID 2

#define WELCOME_SET_MAC_FIRST_ID 4
#define WELCOME_SET_TIME_FIRST_ID 6

#define MAIN_SCREEN_ID 3
#define MAIN_MENU_ALARM 5
#define MAIN_MENU_SETTINGS 7
#define ALARM_SCREEN_ID 9
#define SETTINGS_SCREEN_ID 11
#define ALARM_DETAIL_SCREEN 13
#define SETTINGS_VOLUME_ID 15
#define SETTINGS_SNOOZE_ID 17
#define SETTINGS_MACADRES_ID 19
#define SETTINGS_LANGUAGE_ID 21
#define VOLUME_SCREEN_ID 23
#define SNOOZE_SCREEN_ID 25
#define MACADRES_SCREEN_ID 27
#define LANGUAGE_NL_SCREEN_ID 29
#define  LANGUAGE_EN_SCREEN_ID 31
#define CHANGE_TIME_SETTINGS_ID 33
#define CHANGE_TIME_ID 35
#define MAIN_MENU_RADIO 37
#define RADIO_MENU_STATION1 39
#define RADIO_MENU_STATION2 41
#define RADIO_MENU_STATION3 43
#define RADIO_MENU_OFF 45
#define RADIO_ALARM_ON 47
#define API_SELECT 73
#define API_DATA 75
#define API_CONTENT_DATA 77
#define API_SELECT_NL 79
#define API_SELECT_EN 81
#define ANIMATION_SCREEN 51
#define WEEK_ALARM_SETTINGS_SCREEN_ID 61
#define WEEK_ALARM_SELECT_SCREEN_ID 63
#define WEEK_ALARM_DETAIL_SCREEN_ID 65
#define NTP_AUTO 49
#define SYNTHESIZER 53
#define SYNTHESIZER_SETTINGS_ID 55

#define OK			0
#define NOK			-1



FILE *stream;

void EnterCode(void);
void CodeUp(void);
void CodeDown(void);
void CodeLeft(void);
void CodeRight(void);
//Prototypes for weekalarms
void EnterWeekAlarmScreen(void);
void ChangeWeekAlarmIndexUp(void);
void ChangeWeekAlarmIndexDown(void);
void EnterWeekDetailScreen(void);
void ChangeWeekAlarmUp(void);
void ChangeWeekAlarmDown(void);
void ChangeWeekDetailIndexUp(void);
void ChangeWeekDetailIndexDown(void);
void SaveWeekAlarm(void);

void EnterAlarmDetailScreen(void);
void AlarmOff(void);
void StartSnoozeCheck(void);
void CheckMaxSnoozeTime(void);
void EnterAlarmScreen(void);
void SetDisplayAlarm(void);
void ChangeAlarmDetailNumberUp(void);
void ChangeAlarmDetailNumberDown(void);
void ChangeAlarmDetailTimeDown(void);
void ChangeAlarmDetailTimeUp(void);

/* Prototypes for screen functions */
void EnterScreen(void);
void ExitScreen(void);
void PreviousAlarm(void);
void NextAlarm(void);
void SaveAlarm(void);
void SaveLanguage(void);
void SaveSnooze(void);
void SaveVolume(void);

void ChangeIndexLeft(void);
void ChangeIndexRight(void);

void ChangeNumberUp(void);
void ChangeNumberDown(void);

void ChangeSnoozeUp(void);
void ChangeSnoozeDown(void);

void DisplayTime(void);
void EnterChangeTimeScreen(void);
void ChangeTimeDown(void);
void ChangeTimeUp(void);
void ChangeTimeIndexUp(void);
void ChangeTimeIndexDown(void);
void SetCurrentDateTime(void);

void TurnOnRadio1(void);
void TurnOnRadio2(void);
void TurnOnRadio3(void);

void ConvertArray(void);
void TurnOffRadio(void);

void ChangeVolumeUp(void);
void ChangeVolumeDown(void);
void EnterVolumeScreen(void);

void NtpAutoOn(void);

//Prototypes for changeMacAdres first time
void SaveMAC(void);
void ChangeMacIndexUp(void);
void ChangeMacIndexDown(void);
void ChangeMacNumberUp(void);
void ChangeMacNumberDown(void);
void EnterFirstMacScreen(void);
void EnterChangeTimeFirstScreen(void);
void PrintMacAdres(void);

//Prototypes for changeSnooze
void EnterSnoozeChangeScreen(void);
void StartAnimation(void);
void stopAnimation(void);
void EnterWelcomeScreen(void);
void EnterCodeInput(void);

/* Prototypes for util functions */
void PrintMenuItem(char *lines[]);
void StartMenuStartUp(int alreadyStarted, unsigned char *mac, unsigned char *snooze);
void EnterMacChangeScreen(void);
void SetDateTime(void);

void printApiSourceData(void);
void printApiContentData(void);
void setCountryToEN(void);
void setCountryToNL(void);

/* Global variables */
char TimeStr[5];
char volumeStr[16];
char welcomeSecondstring[16];
char changeVolumeStr[3];
u_short volumeIdx = 0;
char ChangeTimeString[5];
tm Time_gmt;
int ChangeTimeIdx = 2;
int ChangeMacIdx = 0;
int ChangeAlarmTimeIdx = 0;
int AlarmArrayIdx = 0;
int ChangeSnoozeIdx = 0;
char ChangeDateString[10];
struct _alarm TempAlarm[];
struct _alarm TempWeekAlarm[7];
tm Temp_Time;
int CheckMaxAlarmTime= 0;
int CheckNextAlarm = 0;
int CheckMaxSnooze = 0;
tm TimeToCheck;
tm SnoozeTimeCheck;
int SnoozeAlarmTime = 2;

int WeekAlarmDetailIndex = 0;
int WeekAlarmIndex = 0;

//animation values
char animation[16];
int Animationbuf = 0;


void Menu_Update()
{
	SetDateTime();
}

/* Define struct to hold data for a single menu item */
typedef struct {
	unsigned int id;			/* ID for this item */
	unsigned int newId[MAX_MENU_KEY];	/* IDs to jump to on keypress */
	char *Text[LCD_MAX_LINES];		/* Text for this item */
	void(*fpOnKey[MAX_MENU_KEY])(void);	/* Function pointer for each key */
	void(*fpOnEntry)(void);		/* Function called on entry */
	void(*fpOnExit)(void);			/* Function called on exit */
} MENU_ITEM_STRUCT;

MENU_ITEM_STRUCT Menu[] = {
	{ /* Menu item 0 */
		WELCOME_MENU_ID,
		{ /* New item id (Esc, Ok, Up, Down, Left, Right) */
			-1,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{	EnterCodeString,
			" "
		},
		{
			NULL, EnterCode, CodeUp, CodeDown, CodeLeft, CodeRight
		},
		EnterCodeInput,
		ExitScreen
	},
	{
		WELCOME_MENU_FIRST_ID,
		{
			-1,
			WELCOME_SET_MAC_FIRST_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			WelcomeString,
			ClickButtonString
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterWelcomeScreen,
		ExitScreen
	},
	{
		WELCOME_SET_MAC_FIRST_ID,
		{
			-1,
			WELCOME_SET_TIME_FIRST_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			MacWifiString,
			"ABCD"
		},
		{
			NULL, SaveMAC, ChangeMacNumberUp, ChangeMacNumberDown, ChangeMacIndexDown, ChangeMacIndexUp
		},
		EnterFirstMacScreen,
		ExitScreen
	},
	{
		WELCOME_SET_TIME_FIRST_ID,
		{
			-1,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			ChangeTimeWifiString,
			
		},
		
		{
			NULL, SetCurrentDateTime, ChangeTimeUp, ChangeTimeDown, ChangeTimeIndexDown,ChangeTimeIndexUp
		},
		EnterChangeTimeFirstScreen,
		ExitScreen
	},
	{
		MAIN_SCREEN_ID,
		{
			-1,
			MAIN_MENU_ALARM,
			-1,
			-1,
			-1,
			-1
			
		},
		{
			" " ,
			" "
			
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		MAIN_MENU_ALARM,
		{
			MAIN_SCREEN_ID,
			ALARM_SCREEN_ID,
			-1,
			MAIN_MENU_SETTINGS,
			-1,
			-1
		},
		{
			MainMenuString,
			AlarmArrowString
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		MAIN_MENU_SETTINGS,
		{
			MAIN_SCREEN_ID,
			SETTINGS_VOLUME_ID,
			MAIN_MENU_ALARM,
			MAIN_MENU_RADIO,
			-1,
			-1
		},
		{
			AlarmString,
			SettingsArrowString
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	
	{
		ALARM_SCREEN_ID,
		{
			MAIN_SCREEN_ID,
			ALARM_DETAIL_SCREEN,
			-1,
			-1,
			MAIN_MENU_ALARM,
			-1
		},
		{
			AlarmString,
			AlarmListString
		},
		{
			NULL, NULL, NextAlarm, PreviousAlarm, NULL, NULL
		},
		EnterAlarmScreen,
		ExitScreen
	},
	{
		ALARM_DETAIL_SCREEN,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			AlarmDetailString,
			AlarmDetail2String
		},
		{
			NULL, SaveAlarm,ChangeAlarmDetailTimeUp,ChangeAlarmDetailTimeDown,ChangeAlarmDetailNumberUp,ChangeAlarmDetailNumberDown
		},
		EnterAlarmDetailScreen,
		ExitScreen
	},
	{
		SETTINGS_VOLUME_ID,
		{
			MAIN_SCREEN_ID,
			VOLUME_SCREEN_ID,
			-1,
			SETTINGS_SNOOZE_ID,
			-1,
			-1
		},
		{
			SettingsWifiString,
			VolumeArrowString
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		SETTINGS_SNOOZE_ID,
		{
			MAIN_SCREEN_ID,
			SNOOZE_SCREEN_ID,
			SETTINGS_VOLUME_ID,
			SETTINGS_MACADRES_ID,
			-1,
			-1
		},
		{
			VolumeWifiString,
			SnoozeArrowString
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		
		SETTINGS_MACADRES_ID,
		{
			MAIN_SCREEN_ID,
			MACADRES_SCREEN_ID,
			SETTINGS_SNOOZE_ID,
			SETTINGS_LANGUAGE_ID,
			-1,
			-1
		},
		{
			SnoozeWifiString,
			MacArrowString
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		SETTINGS_LANGUAGE_ID,
		{
			MAIN_SCREEN_ID,
			LANGUAGE_NL_SCREEN_ID,
			SETTINGS_MACADRES_ID,
			CHANGE_TIME_SETTINGS_ID,
			-1,
			-1
		},
		{
			MacWifiString,
			LanguageArrowString
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		VOLUME_SCREEN_ID,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			VolumeWifiString,
			volumeStr
		},
		{
			NULL, SaveVolume, ChangeVolumeDown, ChangeVolumeUp, NULL, NULL
		},
		EnterVolumeScreen,
		ExitScreen
	},
	{
		SNOOZE_SCREEN_ID,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			SnoozeWifiString,
			"                "
		},
		{
			NULL, SaveSnooze, ChangeSnoozeUp, ChangeSnoozeDown, NULL, NULL
		},
		EnterSnoozeChangeScreen,
		ExitScreen
		
	},
	{
		MACADRES_SCREEN_ID,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			MacWifiString,
			"                "
		},
		{
			NULL, SaveMAC, ChangeMacNumberUp, ChangeMacNumberDown, ChangeMacIndexDown, ChangeMacIndexUp
		},
		EnterMacChangeScreen,
		ExitScreen
	},
	{
		LANGUAGE_NL_SCREEN_ID,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			LANGUAGE_EN_SCREEN_ID,
			-1,
			-1
		},
		{
			LanguageWifiString,
			LanguageNlArrowString
		},
		{
			NULL, SaveLanguage, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		LANGUAGE_EN_SCREEN_ID,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			LANGUAGE_NL_SCREEN_ID,
			-1,
			-1,
			-1
		},
		{
			LanguageNlWifiString,
			LanguageEnString
		},
		{
			NULL, SaveLanguage, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		CHANGE_TIME_SETTINGS_ID,
		{
			MAIN_SCREEN_ID,
			CHANGE_TIME_ID,
			SETTINGS_LANGUAGE_ID,
			NTP_AUTO,
			-1,
			-1
		},
		{
			LanguageWifiString,
			ChangeTimeSettingsString
		},
		{
			NULL, SaveLanguage, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		CHANGE_TIME_ID, // TIME DING
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			"  ",
			"  "
		},
		{
			NULL, SetCurrentDateTime, ChangeTimeUp, ChangeTimeDown, ChangeTimeIndexDown,ChangeTimeIndexUp
		},
		EnterChangeTimeScreen,
		ExitScreen
	},
	{
		MAIN_MENU_RADIO,
		{
			MAIN_SCREEN_ID,
			RADIO_MENU_STATION1,
			MAIN_MENU_SETTINGS,
			WEEK_ALARM_SETTINGS_SCREEN_ID,
			-1,
			-1
		},
		{
			SettingsWifiString,
			RadioArrowString
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		RADIO_MENU_STATION1,
		{
			MAIN_SCREEN_ID,
			ANIMATION_SCREEN,
			-1,
			RADIO_MENU_STATION2,
			-1,
			-1
		},
		{
			RadioMenuString,
			RadioChannel1ArrowString
		},
		{
			NULL, TurnOnRadio1, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		RADIO_MENU_STATION2,
		{
			MAIN_SCREEN_ID,
			ANIMATION_SCREEN,
			RADIO_MENU_STATION1,
			RADIO_MENU_STATION3,
			-1,
			-1
		},
		{
			RadioChannel1WifiString,
			RadioChannel2ArrowString
		},
		{
			NULL, TurnOnRadio2, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		RADIO_MENU_STATION3,
		{
			MAIN_SCREEN_ID,
			ANIMATION_SCREEN,
			RADIO_MENU_STATION2,
			RADIO_MENU_OFF,
			-1,
			-1
		},
		{
			RadioChannel2WifiString,
			RadioChannel3ArrowString
		},
		{
			NULL, TurnOnRadio3, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		RADIO_MENU_OFF,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			RADIO_MENU_STATION3,
			-1,
			-1,
			-1
		},
		{
			RadioChannel3WifiString,
			RadioOffArrowString
		},
		{
			NULL, TurnOffRadio, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
		
	},

	{
		RADIO_ALARM_ON,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			"ALARM GOES OFF",
			"ESC=SNZ-OK=OFF"
		},
		{
			StartSnoozeCheck, AlarmOff, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		API_SELECT,
		{
			MAIN_SCREEN_ID,
			API_SELECT_NL,
			SYNTHESIZER_SETTINGS_ID,
			-1,
			-1,
			-1
		},
		{
			"  SYNTHESIZER  ",
			">  SHOW NEWS"
			
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		API_SELECT_NL,
		{
			API_SELECT,
			API_DATA,
			-1,
			API_SELECT_EN,
			-1,
			API_CONTENT_DATA
		},
		{
			">   NL NEWS ",
			"  ENGLISH NEWS "
		},
		{
			NULL, ApiData_GetApiData, NULL, NULL, NULL, NULL
		},
		setCountryToNL,
		ExitScreen
	},
	{
		API_SELECT_EN,
		{
			API_SELECT,
			API_DATA,
			API_SELECT_NL,
			-1,
			-1,
			API_CONTENT_DATA
		},
		{
			"    NL NEWS ",
			"> ENGLISH NEWS "
		},
		{
			NULL, ApiData_GetApiData, NULL, NULL, NULL, NULL
		},
		setCountryToEN,
		ExitScreen
	},
	{
		API_DATA,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			API_CONTENT_DATA
		},
		{
			" ",
			" "
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		printApiSourceData,
		ExitScreen
	},
	{
		API_CONTENT_DATA,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			-1,
			API_DATA,
			-1
		},
		{
			" ",
			" "
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		printApiContentData,
		ExitScreen
	},
	
	{
		NTP_AUTO,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			CHANGE_TIME_SETTINGS_ID,
			SYNTHESIZER_SETTINGS_ID,
			-1,
			-1
		},
		{
			ChangeTimeWifiString,
			ChangeNpmAutoString
		},
		{
			NULL, NtpAutoOn, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		WEEK_ALARM_SETTINGS_SCREEN_ID,
		{
			MAIN_SCREEN_ID,
			WEEK_ALARM_SELECT_SCREEN_ID,
			MAIN_MENU_RADIO,
			-1,
			-1,
			-1
		},
		{
			RadioWifiString,
			WeekAlarmArrowString
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		},
		EnterScreen,
		ExitScreen
	},
	{
		WEEK_ALARM_SELECT_SCREEN_ID,
		{
			MAIN_SCREEN_ID,
			WEEK_ALARM_DETAIL_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			WeekAlarmWifiString,
			"MAANDAG 12:05 A"
		},
		{
			NULL, NULL, NULL, NULL, ChangeWeekAlarmIndexDown, ChangeWeekAlarmIndexUp
		},
		EnterWeekAlarmScreen,
		ExitScreen
	},
	{
		WEEK_ALARM_DETAIL_SCREEN_ID,
		{
			MAIN_SCREEN_ID,
			MAIN_SCREEN_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			"MAANDAG",
			"12:05 A"
		},
		{
			NULL, SaveWeekAlarm, ChangeWeekAlarmUp, ChangeWeekAlarmDown, ChangeWeekDetailIndexDown, ChangeWeekDetailIndexUp
		},
		EnterWeekDetailScreen,
		ExitScreen
	}, 
	{
		ANIMATION_SCREEN,
		{
			RADIO_MENU_STATION1,
			RADIO_MENU_STATION1,
			-1,
			-1,
			-1,
			-1
		},
		{
			AnimationChar1,
			AnimationChar2
		},
		{
			stopAnimation, stopAnimation, NULL, NULL, NULL, NULL
		},
		StartAnimation,
		stopAnimation
	},
	{
		SYNTHESIZER_SETTINGS_ID,
		{
			MAIN_SCREEN_ID,
			SYNTHESIZER,
			NTP_AUTO,
			API_SELECT,
			-1,
			-1
		},
		{
			ChangeNpmAutoWifiString,
			">  SYNTHESIZER"
	},
	{
		NULL, NULL, NULL, NULL, NULL, NULL
	},
	EnterScreen,
	ExitScreen
	},
		{
			SYNTHESIZER,
			{
				MAIN_SCREEN_ID,
				-1,
				-1,
				-1,
				-1,
				-1
			},
			{
				"   SYNTHESIZER  ",
				"                "
			},
			{
				NULL, NULL, NULL, NULL, NULL, NULL
			},
			EnterScreen,
			ExitScreen
		}
	
};

void setCountryToEN()
{
	strcpy(newsData.country,"us");
}

void setCountryToNL()
{
	strcpy(newsData.country,"nl");
}
void StartAnimation(){
	
	Animationbuf = 1;
	
}
void stopAnimation (){
	Display_Clear();
	Display_WriteFirstLine(RadioMenuString);
	Animationbuf = 2;
	
}
void wait(int ms){
	int i;
	for (i=0; i<ms;i++){
		_delay_ms(1);
	}
}
void EnterWelcomeScreen(){
	Display_Clear();
	Display_WriteFirstLine(WelcomeString);
	sprintf(welcomeSecondstring, "MAC:%c%c%c%c%c%c%c%c%c%c%c%c",MacChars[MAC[0]], MacChars[MAC[1]],MacChars[MAC[2]],MacChars[MAC[3]],MacChars[MAC[4]],MacChars[MAC[5]], MacChars[MAC[6]], MacChars[MAC[7]], MacChars[MAC[8]], MacChars[MAC[9]], MacChars[MAC[10]], MacChars[MAC[11]]);
	Display_WriteSecondLine(welcomeSecondstring);
	wait(10000);
	Display_Clear();
	Display_WriteFirstLine(WelcomeString);
	sprintf(welcomeSecondstring, "SNOOZE:%cMIN", SnoozeChars[SNOOZE[0]]);
	Display_WriteSecondLine(welcomeSecondstring);
	
	
}
void NtpAutoOn()
{
	tm ntpTime = {0};
	Ethernet_NTPSync(&ntpTime);
	printf("%02d : %02d", ntpTime.tm_hour,ntpTime.tm_min);
	RadioSettings_SetDateTime(ntpTime.tm_year,ntpTime.tm_mon,ntpTime.tm_mday,
							  ntpTime.tm_wday,ntpTime.tm_hour,ntpTime.tm_min,ntpTime.tm_sec);
}

void printApiSourceData(){
	Display_Clear();
	if (newsData.author ==  "null")
	{
		Display_WriteFirstLine("Unknown Author");
	}else{
		Display_WriteFirstLine(newsData.author);
	}
	Display_WriteSecondLine(newsData.pdate);
}

void printApiContentData(){
	Display_Clear();
	
	char *str = newsData.title;
	int len = strlen(str);
	int len1 = len/2;
	int len2 = len - len1; 
	char *s1 = malloc(len1+1); 
	memcpy(s1, str, len1);
	s1[len1] = '\0';
	char *s2 = malloc(len2+1); 
	memcpy(s2, str+len1, len2);
	s2[len2] = '\0';
	
	Display_WriteFirstLine(s1);
	Display_WriteSecondLine(s2);
	
	free(s1);
	free(s2);
}

void ChangeAlarmDetailTimeUp(){
	switch(ChangeAlarmTimeIdx){
		case 0:
		Alarm_alarm[AlarmArrayIdx].on = (Alarm_alarm[AlarmArrayIdx].on + 1) > 1 ? 0 : Alarm_alarm[AlarmArrayIdx].on + 1;
		break;
		case 1:
		Alarm_alarm[AlarmArrayIdx].hour = (Alarm_alarm[AlarmArrayIdx].hour + 1) > 23 ? 0 : Alarm_alarm[AlarmArrayIdx].hour + 1;
		break;
		case 2:
		Alarm_alarm[AlarmArrayIdx].minute = (Alarm_alarm[AlarmArrayIdx].minute + 1) > 59 ? 0 : Alarm_alarm[AlarmArrayIdx].minute + 1;
		break;
	}
	EnterAlarmDetailScreen();
}

void ChangeAlarmDetailTimeDown(){
	switch(ChangeAlarmTimeIdx){
		case 0:
		Alarm_alarm[AlarmArrayIdx].on = (Alarm_alarm[AlarmArrayIdx].on + 1) > 1 ? 0 : Alarm_alarm[AlarmArrayIdx].on + 1;
		break;
		case 1:
		Alarm_alarm[AlarmArrayIdx].hour = (Alarm_alarm[AlarmArrayIdx].hour - 1) < 0 ? 23 : Alarm_alarm[AlarmArrayIdx].hour - 1;
		break;
		case 2:
		Alarm_alarm[AlarmArrayIdx].minute = (Alarm_alarm[AlarmArrayIdx].minute - 1) < 0 ? 59 : Alarm_alarm[AlarmArrayIdx].minute - 1;
		break;
	}
	EnterAlarmDetailScreen();
}

void ChangeAlarmDetailNumberDown(){
	if (ChangeAlarmTimeIdx == 2)
	{
		ChangeAlarmTimeIdx = 0;
	}
	else {
		ChangeAlarmTimeIdx++;
	}
	
}

void ChangeAlarmDetailNumberUp(){
	if (ChangeAlarmTimeIdx == 0)
	{
		ChangeAlarmTimeIdx = 2;
	}
	else {
		ChangeAlarmTimeIdx--;
	}
}

void EnterAlarmDetailScreen(){
	Display_Clear();
	char firstLineDetailAlarm[17];
	char timeDetailAlarm[17];
	char OnOff;
	if(Alarm_alarm[AlarmArrayIdx].on == 1){
		OnOff = 'A';
	}
	else{
		OnOff = 'U';
	}
	sprintf(firstLineDetailAlarm, "   %s %c",Alarm_alarm[AlarmArrayIdx].alarmName, OnOff);
	sprintf(timeDetailAlarm, "     %02d:%02d", Alarm_alarm[AlarmArrayIdx].hour, Alarm_alarm[AlarmArrayIdx].minute);
	Display_WriteFirstLine(firstLineDetailAlarm);
	Display_WriteSecondLine(timeDetailAlarm);
}

void EnterAlarmScreen(){
	AlarmArrayIdx = 0;
	Display_Clear();
	Display_WriteFirstLine(AlarmString);
	SetDisplayAlarm();
}

void SetDisplayAlarm(){
	char DisplayAlarmString[17];
	char OnOff;
	//struct Alarm_alarm Alarm = Alarm_alarm[AlarmArrayIdx];
	if(Alarm_alarm[AlarmArrayIdx].on == 1){
		OnOff = 'A';
	}
	else{
		OnOff = 'U';
	}
	sprintf(DisplayAlarmString, ">%s %i:%i %c", Alarm_alarm[AlarmArrayIdx].alarmName, Alarm_alarm[AlarmArrayIdx].hour, Alarm_alarm[AlarmArrayIdx].minute, OnOff);
	Display_WriteSecondLine(DisplayAlarmString);
}


void SetCurrentDateTime()
{
	RadioSettings_SetDateTime(Temp_Time.tm_year,Temp_Time.tm_mon,Temp_Time.tm_mday,0,Temp_Time.tm_hour,Temp_Time.tm_min,0);
	RadioSettings_GetDateTime(&Time_gmt);
}

void TurnOffRadio(){
	Shoutcast_StopStream();
	
}

/*
Connect with jazz radio
*/
void TurnOnRadio1()
{
	RadioStream Rs = { .ipAddress = "136.144.135.194", .port = 443, .url = "/alldayjazz"};
	int ResultConnect = Shoutcast_ConnectToStream(Rs);
	printf("%d", ResultConnect);
	if (ResultConnect == 0)
	Shoutcast_PlayStream();
}

/*
Connect with classic rock radio
*/
void TurnOnRadio2()
{
	RadioStream Rs = { .ipAddress = "91.221.150.18", .port = 80, .url = "/arrow"};
	
	int ResultConnect = Shoutcast_ConnectToStream(Rs);
	if (ResultConnect == 0)
	Shoutcast_PlayStream();
}

/*
Connect with aardschok radio
*/
void TurnOnRadio3()
{
	RadioStream rs = { .ipAddress = "37.247.41.53", .port = 80, .url = "/Aardschok192.mp3"};
	int ResultConnect = Shoutcast_ConnectToStream(rs);
	if (ResultConnect == 0)
	Shoutcast_PlayStream();
}

/*
Refresh for changeTimeString and changeDateString. They have the updated values now
*/
void DisplayTime()
{
	Display_Clear();
	
	sprintf(ChangeDateString,"%02d/%02d/%2d", (int)Time_gmt.tm_mday, (int)Time_gmt.tm_mon, (int)Time_gmt.tm_year);
	sprintf(TimeStr, "%02d:%02d", Time_gmt.tm_hour, Time_gmt.tm_min);
	Display_WriteFirstLine(ChangeDateString);
	Display_WriteSecondLine(TimeStr);
}

void ChangeTimeIndexUp()
{
	if (ChangeTimeIdx == 4) /* If the last index is reached the index returns to 0 */
	{
		ChangeTimeIdx = 0;
		}else {
		ChangeTimeIdx++;
	}
	DisplayTimeTemp();
}

void ChangeTimeIndexDown()
{
	if(ChangeTimeIdx == 0){ /* If the 0 is reached the index returns to 4 */
		ChangeTimeIdx = 4;
	}
	else{
		ChangeTimeIdx--;
	}
	DisplayTimeTemp();
}

/*
Manipulates the time_gmt. hour, min, day and month have max values they can reach, if they exceed this they return to the min value
*/
void ChangeTimeUp()
{
	switch(ChangeTimeIdx){
		case 0:
			Temp_Time.tm_hour = (Temp_Time.tm_hour + 1) > 23 ? 0 : Temp_Time.tm_hour + 1;
			break;
		case 1:
			Temp_Time.tm_min =(Temp_Time.tm_min + 1) > 59 ? 0 : Temp_Time.tm_min + 1;
			break;
		case 2:
			if(Temp_Time.tm_year % 4 == 0)
				Temp_Time.tm_mday = (Temp_Time.tm_mday + 1) > MaxDaysMonthLeapYear[(Temp_Time.tm_mon-1)] ? 01 : Temp_Time.tm_mday + 1;
			else
				Temp_Time.tm_mday = (Temp_Time.tm_mday + 1) > MaxDaysMonth[(Temp_Time.tm_mon-1)] ? 01 : Temp_Time.tm_mday + 1;
			break;
		case 3:
			Temp_Time.tm_mon = (Temp_Time.tm_mon + 1) > 12 ? 01 : Temp_Time.tm_mon + 1;
			CheckMaxMonthDay();
			break;
		case 4:
			Temp_Time.tm_year++; 
			CheckMaxMonthDay();
			break;
	}
	
	DisplayTimeTemp();
}

/*
Manipulates the time_gmt. hour, min, day and month have min values they can reach, if they exceed this they return to the max value
*/
void ChangeTimeDown()
{
	switch(ChangeTimeIdx){
		case 0:
			Temp_Time.tm_hour = (Temp_Time.tm_hour - 1) < 0 ? 23 : Temp_Time.tm_hour - 1;
			break;
		case 1:
			Temp_Time.tm_min =(Temp_Time.tm_min - 1) < 0 ? 59 : Temp_Time.tm_min - 1;
			break;
		case 2:
			if(Temp_Time.tm_year % 4 == 0)
				Temp_Time.tm_mday = (Temp_Time.tm_mday - 1) < 1 ? MaxDaysMonthLeapYear[(Temp_Time.tm_mon-1)] : Temp_Time.tm_mday - 1;
			else
				Temp_Time.tm_mday = (Temp_Time.tm_mday - 1) < 1 ? MaxDaysMonth[(Temp_Time.tm_mon-1)] : Temp_Time.tm_mday - 1;
			break;
		case 3:
			Temp_Time.tm_mon = (Temp_Time.tm_mon - 1) < 1 ? 12 : Temp_Time.tm_mon - 1;
			CheckMaxMonthDay();
			break;
		case 4:
			Temp_Time.tm_year--;
			CheckMaxMonthDay();
			break;
	}
	
	DisplayTimeTemp();
}


void CheckMaxMonthDay(){
	if(Temp_Time.tm_year % 4 == 0)
		Temp_Time.tm_mday = (Temp_Time.tm_mday) > MaxDaysMonthLeapYear[(Temp_Time.tm_mon)-1] ? MaxDaysMonthLeapYear[(Temp_Time.tm_mon)-1] : Temp_Time.tm_mday;
	else
		Temp_Time.tm_mday = (Temp_Time.tm_mday) > MaxDaysMonth[(Temp_Time.tm_mon)-1] ? MaxDaysMonth[(Temp_Time.tm_mon)-1] : Temp_Time.tm_mday;
}
	

/*
changeTimeIdx is reset to 0, this means the hour is manipulated first
time_gmt is set to the correct values
*/
void EnterChangeTimeScreen()
{
	ChangeTimeIdx = 2;
	RadioSettings_GetDateTime(&Temp_Time);
	Temp_Time.tm_year = Temp_Time.tm_year+1900;
	Temp_Time.tm_mon = Temp_Time.tm_mon+1;
	DisplayTimeTemp();
}

void DisplayTimeTemp(){
	Display_Clear();
	
	sprintf(ChangeDateString,"%02d/%02d/%2d", (int)Temp_Time.tm_mday, (int)Temp_Time.tm_mon, (int)Temp_Time.tm_year);
	sprintf(TimeStr, "%02d:%02d", Temp_Time.tm_hour, Temp_Time.tm_min);
	Display_WriteFirstLine(ChangeDateString);
	Display_WriteSecondLine(TimeStr);
}

/*
The time is set to 26/02/2019 as default and can be changed by the user.
*/
void EnterChangeTimeFirstScreen()
{
	RadioSettings_GetDateTime(&Time_gmt);
	Time_gmt.tm_mday = 26;
	Time_gmt.tm_mon = 2;
	Time_gmt.tm_year = 2019;
	DisplayTime();
}

void EnterFirstMacScreen()
{
	PrintMacAdres();
}

void SaveMAC()
{
	char MacPrint[12];
	sprintf(MacPrint, "%c%c%c%c%c%c%c%c%c%c%c%c",MacChars[MAC[0]], MacChars[MAC[1]],MacChars[MAC[2]],MacChars[MAC[3]],MacChars[MAC[4]],MacChars[MAC[5]], MacChars[MAC[6]], MacChars[MAC[7]], MacChars[MAC[8]], MacChars[MAC[9]], MacChars[MAC[10]], MacChars[MAC[11]]);
	printf("what is the mac thats saved: %s\n", MacPrint);
	unsigned char *Mac = MacPrint;
	printf("what is the pointer: %s\n", Mac);
	
	FlashDrive_WriteToFlashDrive(MAC_FLASH_ID, Mac, sizeof(unsigned char) * 12);
	/*At45dbPageWrite(PAGE_NUMBER, Mac, sizeof(unsigned char) * 24);
	At45dbPageWrite(PAGE_NUMBER, Mac, sizeof(unsigned char) * 11);*/
}

void ChangeMacNumberDown()
{
	if(MAC[ChangeMacIdx] == 0){ /* If the 0 is reached the index returns to 15 */
		MAC[ChangeMacIdx] = 15;
	}
	else{
		MAC[ChangeMacIdx]--;
	}
	PrintMacAdres();
}

void ChangeMacNumberUp()
{
	if(MAC[ChangeMacIdx] == 15){ /* If the max value is reached the index returns to 0 */
		MAC[ChangeMacIdx] = 0;
	}
	else{
		MAC[ChangeMacIdx]++;
	}
	PrintMacAdres();
}

void PrintMacAdres()
{
	Display_Clear();
	char MacPrint[12];
	sprintf(MacPrint, "%c%c%c%c%c%c%c%c%c%c%c%c",MacChars[MAC[0]], MacChars[MAC[1]],MacChars[MAC[2]],MacChars[MAC[3]],MacChars[MAC[4]],MacChars[MAC[5]], MacChars[MAC[6]], MacChars[MAC[7]], MacChars[MAC[8]], MacChars[MAC[9]], MacChars[MAC[10]], MacChars[MAC[11]]);
	Display_WriteFirstLine(MacWifiString);
	Display_WriteSecondLine(MacPrint);
}

/*
converts the MAC array from chars('0'...'F') to ints(0...15) so they can be used as an array index
*/
void ConvertArray()
{
	
	int Size = sizeof(MAC);
	int i = 0;
	
	for(i = 0; i < Size; i++) {
		
		switch (MAC[i])
		{
			case '0':
			MAC[i] = 0;
			break;
			case '1':
			MAC[i] = 1;
			break;
			case '2':
			MAC[i] = 2;
			break;
			case '3':
			MAC[i] = 3;
			break;
			case '4':
			MAC[i] = 4;
			break;
			case '5':
			MAC[i] = 5;
			break;
			case '6':
			MAC[i] = 6;
			break;
			case '7':
			MAC[i] = 7;
			break;
			case '8':
			MAC[i] = 8;
			break;
			case '9':
			MAC[i] = 9;
			break;
			case 'A':
			MAC[i] = 10;
			break;
			case 'B':
			MAC[i] = 11;
			break;
			case 'C':
			MAC[i] = 12;
			break;
			case 'D':
			MAC[i] = 13;
			break;
			case 'E':
			MAC[i] = 14;
			break;
			case 'F':
			MAC[i] = 15;
			break;}
		
	}
	
}

void ChangeMacIndexDown()
{
	if(ChangeMacIdx == 0){ /* If the 0 is reached the index returns to 11 */
		ChangeMacIdx = 11;
	}
	else{
		ChangeMacIdx--;
	}
}

void ChangeMacIndexUp()
{
	if(ChangeMacIdx == 11){ /* If the max value is reached the index returns to 0 */
		ChangeMacIdx = 0;
	}
	else{
		ChangeMacIdx++;
	}
}

void EnterMacChangeScreen()
{
	PrintMacAdres();
}

void SaveLanguage()
{
	
}
void ConvertSnoozeArray(){
	int Size = sizeof(SNOOZE);
	int i = 0;
	for(i = 0; i < Size; i++) {
		switch (SNOOZE[i])
		{
			case '1':
			SnoozeAlarmTime = 1;
			SNOOZE[i] = 0;
			break;
			case '2':
			SnoozeAlarmTime = 2;
			SNOOZE[i] = 1;
			break;
			case '3':
			SnoozeAlarmTime = 3;
			SNOOZE[i] = 2;
			break;
			case '4':
			SnoozeAlarmTime = 4;
			SNOOZE[i] = 3;
			break;
			case '5':
			SnoozeAlarmTime = 5;
			SNOOZE[i] = 4;
			break;
			
		}
	}
}

void SaveSnooze()
{
	char SnoozePrint[12];
	sprintf(SnoozePrint, "%c",SnoozeChars[SNOOZE[0]]);
	printf("what is the Snooze thats saved: %s\n", SnoozePrint);
	unsigned char *Snooze = SnoozePrint;
	printf("what is the Snooze pointer: %s\n", Snooze);
	At45dbPageWrite(PAGE_NUMBER_SNOOZE, Snooze, sizeof(unsigned char) * 2);
	//SNOOZE[0] = SnoozePrint;
	//ConvertSnoozeArray();
}

void ChangeSnoozeUp()
{
	if(SNOOZE[ChangeSnoozeIdx] == 4){ /* If the max value is reached the index returns to 0 */
		SNOOZE[ChangeSnoozeIdx] = 0;
	}
	else{
		SNOOZE[ChangeSnoozeIdx]++;
	}
	PrintSnoozeAdres();
	
}


void ChangeSnoozeDown()
{
	if(SNOOZE[ChangeSnoozeIdx] == 0){ /* If the 0 is reached the index returns to 4 */
		SNOOZE[ChangeSnoozeIdx] = 4;
	}
	else{
		SNOOZE[ChangeSnoozeIdx]--;
	}
	PrintSnoozeAdres();
}

void PrintSnoozeAdres()
{
	Display_Clear();
	char SnoozePrint[1];
	sprintf(SnoozePrint, "%c MIN", SnoozeChars[SNOOZE[0]]);
	Display_WriteFirstLine(SnoozeWifiString);
	Display_WriteSecondLine(SnoozePrint);
}
void EnterSnoozeChangeScreen(){
	PrintSnoozeAdres();
}

void SaveVolume()
{
	
}

void ChangeVolumeUp()
{
	u_short volume = RadioSettings_GetVolume();
	printf("Volume up: %u\n", volume);
	int TempVol = volume/256;
	TempVol += 5;
	
	
	if (TempVol <= 100)
	{
		Display_Clear();
		sprintf(volumeStr, "      %u      ", 100-TempVol);
		Display_WriteFirstLine(VolumeWifiString);
		Display_WriteSecondLine(volumeStr);
		volume += 5;
		u_char *volChar = (u_char*)&volume;
		RadioSettings_SetVolume(*volChar);
	}
}
void ChangeVolumeDown()
{
	u_short volume = RadioSettings_GetVolume();
	
	
	int TempVol = volume/256;
	TempVol -= 5;
	
		
	if (TempVol > 0)
	{
		Display_Clear();
		//volumeIdx = volumeIdx - 2;
		sprintf(volumeStr, "      %u      ", 100-TempVol);
		
		Display_WriteFirstLine(VolumeWifiString);
		Display_WriteSecondLine(volumeStr);
		
		volume -= 5;
		u_char *volChar = (u_char *)&volume;
		RadioSettings_SetVolume(*volChar);
	}
}

void AlarmOff(){
	printf("alarm off clicked\n");
	radio_alarmAlarmOn = 0;
	CheckMaxAlarmTime = 0;
}

void StartSnoozeCheck(){
	printf("alarm is snoozed\n");
	radio_alarmAlarmOn = 0;
	CheckMaxSnooze = 1;
	CheckMaxAlarmTime = 0;
	//CheckMaxAlarmTimeMenu();
	CheckNextAlarmTime();
}

void ChangeNumberUp()
{
	
}
void ChangeNumberDown()
{
	
}

void ChangeIndexLeft()
{
	
}
void ChangeIndexRight()
{
	
}

void SaveAlarm()
{
	Alarm_SaveAlarm();
}

void NextAlarm()
{
	AlarmArrayIdx++;
	if(AlarmArrayIdx == 5){
		AlarmArrayIdx = 0;
	}
	Display_Clear();
	Display_WriteFirstLine(AlarmString);
	SetDisplayAlarm();
}

void PreviousAlarm()
{
	AlarmArrayIdx--;
	if(AlarmArrayIdx == -1){
		AlarmArrayIdx = 4;
	}
	Display_Clear();
	Display_WriteFirstLine(AlarmString);
	SetDisplayAlarm();
}

void EnterScreen()
{
}

void ExitScreen()
{
	LcdClearDisplay();
}

void EnterVolumeScreen()
{
	volumeIdx = RadioSettings_GetVolume();
	int volume = 100-(volumeIdx/256);
	sprintf(volumeStr, "      %u      ", volume);
	Display_WriteSecondLine(volumeStr);
}

void EnterWeekAlarmScreen(){
	Display_Clear();
	Display_WriteFirstLine(WeekAlarmWifiString);
	
	char WeekAlarmOnChar;
	if(TempWeekAlarm[WeekAlarmIndex].on == 1){
		WeekAlarmOnChar = 'A';
	}
	else{
		WeekAlarmOnChar = 'U';
	}
	
	char weekAlarmString[17];
	sprintf(weekAlarmString, "%s %02i:%02i %c", Week_alarms[WeekAlarmIndex].alarmName, Week_alarms[WeekAlarmIndex].hour, Week_alarms[WeekAlarmIndex].minute, WeekAlarmOnChar);
	Display_WriteSecondLine(weekAlarmString);
}

void ChangeWeekAlarmIndexUp(){
	WeekAlarmIndex++;
	if(WeekAlarmIndex == 7){
		WeekAlarmIndex = 0;
	}
	EnterWeekAlarmScreen();
}

void ChangeWeekAlarmIndexDown(){
	WeekAlarmIndex--;
	if(WeekAlarmIndex == -1){
		WeekAlarmIndex = 6;
	}
	EnterWeekAlarmScreen();
}

/*
Sets the TempWeekAlarm array which is used when modifications are made to the alarms
*/
void EnterWeekDetailScreen(){
	memcpy(TempWeekAlarm, Week_alarms, sizeof(_alarm)*7);
	ShowWeekDetailScreen();
}

/*
Displays the information of the selected WeekAlarm 
WeekAlarmOnChar:		The char that is set to 'A' if the alarm is on and 'U' if the alarm is of
weekAlarmString:		The charArray used to store the name and status of the alarm
weekAlarmTimeString:	The charArray used to store the time of the alarm
*/
void ShowWeekDetailScreen(){
	Display_Clear();
	//Check if the alarm is on
	char WeekAlarmOnChar;
	if(TempWeekAlarm[WeekAlarmIndex].on == ALARM_ON){
		WeekAlarmOnChar = 'A';
	}
	else{
		WeekAlarmOnChar = 'U';
	}
	//Set the strings with the correct values
	char weekAlarmString[17];
	sprintf(weekAlarmString, "  %s  %c", TempWeekAlarm[WeekAlarmIndex].alarmName, WeekAlarmOnChar);
	char weekAlarmTimeString[17];
	sprintf(weekAlarmTimeString, "     %02i:%02i", TempWeekAlarm[WeekAlarmIndex].hour, TempWeekAlarm[WeekAlarmIndex].minute);
	//Write to the display
	Display_WriteFirstLine(weekAlarmString);
	Display_WriteSecondLine(weekAlarmTimeString);
}

/*
WeekAlarmDetailIndex: Determines which part of the alarm can be modified
*/
void ChangeWeekDetailIndexUp(){
	if(WeekAlarmDetailIndex == MAX_WEEKALARM_INDEX){
		WeekAlarmDetailIndex = 0;
	}
	else{
	WeekAlarmDetailIndex++;
	}
}

/*
WeekAlarmDetailIndex: Determines which part of the alarm can be modified
*/
void ChangeWeekDetailIndexDown(){
	if(WeekAlarmDetailIndex == MIN_WEEKALARM_INDEX){
		WeekAlarmDetailIndex = 2;
	}
	else{
	WeekAlarmDetailIndex--;
	}
}

/*
WeekAlarmDetailIndex:	Determines which part of the alarm can be modified
WeekAlarmIndex:			Determines which alarm of the array is modified
*/
void ChangeWeekAlarmUp(){
	switch(WeekAlarmDetailIndex){
		case 0: 
			//Change the on status of the alarm (0 or 1)
			TempWeekAlarm[WeekAlarmIndex].on = (TempWeekAlarm[WeekAlarmIndex].on + 1) > 1 ? 0 : 1;
			break;
		case 1:
			//Increment the hour or set it to 0 when it would reach 24
			TempWeekAlarm[WeekAlarmIndex].hour = (TempWeekAlarm[WeekAlarmIndex].hour + 1) > MAX_HOUR_VALUE ? 0 : TempWeekAlarm[WeekAlarmIndex].hour + 1;
			break;
		case 2:
			//Increment the minute or set it to 0 when it would reach 60
			TempWeekAlarm[WeekAlarmIndex].minute = (TempWeekAlarm[WeekAlarmIndex].minute + 1) > MAX_MINUTE_VALUE ? 0 : TempWeekAlarm[WeekAlarmIndex].minute+ 1;
			break;
		default:
			break;
	}
	ShowWeekDetailScreen();
}

/*
WeekAlarmDetailIndex:	Determines which part of the alarm can be modified
WeekAlarmIndex:			Determines which alarm of the array is modified
*/
void ChangeWeekAlarmDown(){
	switch(WeekAlarmDetailIndex){
		case 0:
			//Change the on status of the alarm (0 or 1)
			TempWeekAlarm[WeekAlarmIndex].on = (TempWeekAlarm[WeekAlarmIndex].on + 1) > 1 ? 0 : 1;
			break;
		case 1:
			//Decrement the hour or set it to 23 when it would reach -1
			TempWeekAlarm[WeekAlarmIndex].hour = (TempWeekAlarm[WeekAlarmIndex].hour - 1) < 0 ? MAX_HOUR_VALUE : TempWeekAlarm[WeekAlarmIndex].hour - 1;
			break;
		case 2:
			//Decrement the minute or set it to 59 when it would reach -1
			TempWeekAlarm[WeekAlarmIndex].minute = (TempWeekAlarm[WeekAlarmIndex].minute - 1) < 0 ? MAX_MINUTE_VALUE : TempWeekAlarm[WeekAlarmIndex].minute - 1;
			break;
		default:
			break;
	}
	ShowWeekDetailScreen();
}

/*
Week_alarms are set to the (modified) alarm settings from TempWeekAlarm
*/
void SaveWeekAlarm(){
	memcpy(Week_alarms, TempWeekAlarm, sizeof(_alarm)*7);
	radio_week_alarm_SaveWeekAlarms();
}

static unsigned int CurrentItem = 0; /* Start at first menu item */
static unsigned int CurrentId;		 /* The current id of the menu item*/

void HandleMenu(int Key)
{
	printf("reached with currentid: %d", CurrentId);
	if(CurrentId == 53){
		if(Key == CLICK_KEY_ESC) {
			
			/* Esc */ 
			if (NULL != Menu[CurrentItem].fpOnKey[MENU_KEY_ESC]) {
				(*Menu[CurrentItem].fpOnKey[MENU_KEY_ESC])();
			}
			CurrentId = Menu[CurrentItem].newId[MENU_KEY_ESC];
			if (NULL != Menu[CurrentItem].fpOnExit && CurrentId != SAME_SCREEN) {
				(*Menu[CurrentItem].fpOnExit)();
			}
		}
			else{
				synthesizer_handleKeys(Key);
			}
	}
	else{
	switch (Key) {
		/* Esc */
		case CLICK_KEY_ESC:
		if (NULL != Menu[CurrentItem].fpOnKey[MENU_KEY_ESC]) {
			(*Menu[CurrentItem].fpOnKey[MENU_KEY_ESC])();
		}
		CurrentId = Menu[CurrentItem].newId[MENU_KEY_ESC];
		if (NULL != Menu[CurrentItem].fpOnExit && CurrentId != SAME_SCREEN) {
			(*Menu[CurrentItem].fpOnExit)();
		}
		
		break;
		/* Ok */
		case CLICK_KEY_OK:
		if (NULL != Menu[CurrentItem].fpOnKey[MENU_KEY_OK]) {
			(*Menu[CurrentItem].fpOnKey[MENU_KEY_OK])();
		}
		if (Lockscreen_Authenticated() != 1)
			break;
		CurrentId = Menu[CurrentItem].newId[MENU_KEY_OK];
		if (NULL != Menu[CurrentItem].fpOnExit && CurrentId != SAME_SCREEN) {
			(*Menu[CurrentItem].fpOnExit)();
		}
		
		break;
		/* Up */
		case CLICK_KEY_UP:
		if (NULL != Menu[CurrentItem].fpOnKey[MENU_KEY_UP]) {
			(*Menu[CurrentItem].fpOnKey[MENU_KEY_UP])();
		}
		CurrentId = Menu[CurrentItem].newId[MENU_KEY_UP];
		if (NULL != Menu[CurrentItem].fpOnExit && CurrentId != SAME_SCREEN) {
			(*Menu[CurrentItem].fpOnExit)();
		}
		
		break;
		/* Down */
		case CLICK_KEY_DOWN:
		if (NULL != Menu[CurrentItem].fpOnKey[MENU_KEY_DOWN]) {
			(*Menu[CurrentItem].fpOnKey[MENU_KEY_DOWN])();
		}
		CurrentId = Menu[CurrentItem].newId[MENU_KEY_DOWN];
		if (NULL != Menu[CurrentItem].fpOnExit && CurrentId != SAME_SCREEN) {
			(*Menu[CurrentItem].fpOnExit)();
		}
		
		break;
		/* Left */

		case CLICK_KEY_LEFT:
		if (NULL != Menu[CurrentItem].fpOnKey[MENU_KEY_LEFT]) {
			(*Menu[CurrentItem].fpOnKey[MENU_KEY_LEFT])();
		}
		CurrentId = Menu[CurrentItem].newId[MENU_KEY_LEFT];
		if (NULL != Menu[CurrentItem].fpOnExit && CurrentId != SAME_SCREEN) {
			(*Menu[CurrentItem].fpOnExit)();
		}
		
		break;
		/* Right */

		case CLICK_KEY_RIGHT:
		if (NULL != Menu[CurrentItem].fpOnKey[MENU_KEY_RIGHT]) {
			(*Menu[CurrentItem].fpOnKey[MENU_KEY_RIGHT])();
		}
		CurrentId = Menu[CurrentItem].newId[MENU_KEY_RIGHT];
		if (NULL != Menu[CurrentItem].fpOnExit && CurrentId != SAME_SCREEN) {
			(*Menu[CurrentItem].fpOnExit)();
		}
		
		break;
		default:
		break;
	}
		
	}
	
	/* Lookup new current menu id in the array of menu items */
	if (CurrentId != -1) 
	{
		CurrentItem = 0;
		
		printf("Current id: %d\n", CurrentId);
		while (Menu[CurrentItem].id != CurrentId) 
		{
			printf("Current item: %d\n", CurrentItem);
			CurrentItem += 1;
		}
		/* Display the menu item text */
		PrintMenuItem(Menu[CurrentItem].Text);
		/* Call entry function */
		if (NULL != Menu[CurrentItem].fpOnEntry) {
			(*Menu[CurrentItem].fpOnEntry)();
		}
	}
	else {
		if (MAIN_SCREEN_ID == CurrentId)
		{
			PrintMenuItem(Menu[CurrentItem].Text);
		}
		printf("same screen\n");
	}
}


void PrintMenuItem(char *Lines[])
{
	Display_WriteFirstLine(Lines[0]);
	Display_WriteSecondLine(Lines[1]);
	
}

void SetDateTime()
{
	if(CheckMaxAlarmTime == 1){
		if(Time_gmt.tm_hour == TimeToCheck.tm_hour && Time_gmt.tm_min == TimeToCheck.tm_min){
			radio_alarmAlarmOn = 0;
			printf("Alarm stopped");
		}
	}
	if(CheckMaxSnooze == 1){
		if(Time_gmt.tm_hour == SnoozeTimeCheck.tm_hour && Time_gmt.tm_min == SnoozeTimeCheck.tm_min){
			radio_alarmAlarmOn = 1;
			printf("Alarm snoozed");
			CheckMaxSnooze = 0;
			CheckNextAlarmTime();
			CurrentId = RADIO_ALARM_ON;
			Display_Clear();
			CheckMaxAlarmTimeMenu();
			HandleMenu(-2); //magic number for no key pressed
		}
	}
	
	RadioSettings_GetDateTime(&Time_gmt);
	Time_gmt.tm_mon = Time_gmt.tm_mon + 1;
	Time_gmt.tm_year = Time_gmt.tm_year + 1900;
	if (CurrentId == MAIN_SCREEN_ID){
		Display_Clear();
		sprintf(ChangeDateString,"%02d/%02d/%2d", (int)Time_gmt.tm_mday, (int)Time_gmt.tm_mon, (int)Time_gmt.tm_year);
		sprintf(TimeStr, "%02d:%02d", Time_gmt.tm_hour, Time_gmt.tm_min);
		Display_WriteFirstLine(ChangeDateString);
		Display_WriteSecondLine(TimeStr);

	}
}


void Menu_AlarmGoesOf(){
	CurrentId = RADIO_ALARM_ON;
	Display_Clear();
	CheckMaxAlarmTimeMenu();
	HandleMenu(-2); //magic number for no key pressed
}

void CheckMaxAlarmTimeMenu(){
	CheckMaxAlarmTime = 1;
	TimeToCheck.tm_hour = Time_gmt.tm_hour;
	TimeToCheck.tm_min = Time_gmt.tm_min + MAX_ALARM_TIME;
	if(TimeToCheck.tm_min >= 60){
		TimeToCheck.tm_min = TimeToCheck.tm_min %60;
		TimeToCheck.tm_hour = (TimeToCheck.tm_hour + 1) > 23 ? 0 : TimeToCheck.tm_hour + 1;
	}
}

void CheckNextAlarmTime(){
	CheckNextAlarm = 1;
	SnoozeTimeCheck.tm_hour = Time_gmt.tm_hour;
	SnoozeTimeCheck.tm_min = Time_gmt.tm_min + SnoozeAlarmTime;
	if(SnoozeTimeCheck.tm_min >= 60){
		SnoozeTimeCheck.tm_min = SnoozeTimeCheck.tm_min %60;
		SnoozeTimeCheck.tm_hour = (SnoozeTimeCheck.tm_hour + 1) > 23 ? 0 : SnoozeTimeCheck.tm_hour + 1;
	}
}



/*
mac is the address received by the main loop and is assigned to MAC
alreadyStarted determines on which screen the menu starts
*/
void StartMenuStartUp(int alreadyStarted, unsigned char *Mac, unsigned char *Snooze)
{
	RadioSettings_GetDateTime(&Time_gmt);
	Time_gmt.tm_mon = Time_gmt.tm_mon + 1;
	Time_gmt.tm_year = Time_gmt.tm_year + 1900;
	Display_Clear();
	strncpy(MAC, Mac, 12);
	ConvertArray();
	strncpy (SNOOZE, Snooze, 1);
	ConvertSnoozeArray();
	if(alreadyStarted == 0){
		CurrentItem = 0;
		PrintMenuItem(Menu[0].Text);
		printf("\nde start is bereikt met: %s", Menu[0].Text[0]);
	}
	else{
		LcdBackLight(LCD_BACKLIGHT_ON);
		CurrentItem = 1;
		PrintMenuItem(Menu[1].Text);
	}
	//RadioSettings_GetDateTime(&Time_gmt);
}



static int lockscreenCode[PASSCODE_SIZE] = {0, 0, 0, 0};
static uint8_t codeIndex;


void CodeArrayToString(int arr[], uint8_t size, char str[])
{
	uint8_t i;
	int index = 0;
	
	for (i = 0; i < size; i++)
		index += sprintf(&str[index], "%d", arr[i]);

}

void PrintLockscreenCode()
{
	char str[PASSCODE_SIZE];
	//IntArrayToString(lockscreenCode, 4, str);
	CodeArrayToString(lockscreenCode, PASSCODE_SIZE, str);
	Display_WriteSecondLine(str);
}
void EnterCodeInput()
{
	printf("Code input enter\n");
	Display_Clear();
	Display_WriteFirstLine("Voer code in");
	PrintLockscreenCode();	
}
void EnterCode()
{
	int8_t result = Lockscreen_Unlock(lockscreenCode);
	if (result == 1)
	{
		printf("Passcode valid\n");
	}
	else if (result == 0)
	{
		printf("Passcode invalid\n");
		Display_Clear();
		Display_WriteFirstLine("Code incorrect!");
		PrintLockscreenCode();
	}
	else
		printf("Error code\n");
}

void CodeUp()
{
	int val = lockscreenCode[codeIndex];
	if (++val > 9)
		val = 0;
	lockscreenCode[codeIndex] = val;
	Display_Clear();
	Display_WriteFirstLine("Voer code in");
	PrintLockscreenCode();
}
void CodeDown()
{
	int val = lockscreenCode[codeIndex];
	if (--val < 0)
		val = 9;
	lockscreenCode[codeIndex] = val;
	
	Display_Clear();
	Display_WriteFirstLine("Voer code in");
	PrintLockscreenCode();
	
}
void CodeLeft() 
{
	if (--codeIndex < 0)
		codeIndex = 0;
}
void CodeRight() 
{
	if (++codeIndex > 3)
		codeIndex = 3;
}
