/*
 * lockscreen.c
 *
 * Created: 21-3-2019 14:55:40
 *  Author: Dion van der Linden
 */ 

#include "application/lockscreen.h"
#include "control/flash_drive.h"
#include "control/radio_display.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

/*#define AES256	32
#define IV_SIZE 16
#define PASSCODE_SIZE 4

#define TRUE 0
#define FALSE !TRUE
typedef int BOOL;*/


int CodeArrayToCode(int arr[],  uint8_t size);

static int8_t authenticated = 0;
 
// Initializes the lockscreen.
void Lockscreen_Init(void)
{
	authenticated = 0;
}

// Unlocks the lockscreen with given code.
// Returns 0 if code is invalid
// Returns 1 if code is valid
// returns -1 if error was encountered
int8_t Lockscreen_Unlock(int codeArr[PASSCODE_SIZE])
{
	int8_t result = Lockscreen_IsPasscodeValid(codeArr);
	
	if (result == 1)
	{
		printf("Passcode valid\n");
		authenticated = 1;
	}
	else if (result == 0)
	{
		printf("Passcode invalid\n");
	}
	return result;	
}

// Convert array to single integer.
int CodeArrayToCode(int arr[],  uint8_t size)
{
	int result = 0;

	uint8_t i;
	for (i = 0; i < size; i++)
	{
		result = 10 * result + arr[i];
	}
	return result;
}


// Updates the lockscreen.
void Lockscreen_Update(u_char key)
{
	//if (lockscreenEnabled == B_FALSE) return;
	//
	//Display_Clear();
	//Display_WriteFirstLine("Enter code:");
	//
	//char nr[4];
	//char nr1[1];
	//itoa(indexNumbers[0], nr1, 10);
	//strcat(nr, nr1);
	//itoa(indexNumbers[1], nr1, 10);
	//strcat(nr, nr1);
	//itoa(indexNumbers[2], nr1, 10);
	//strcat(nr, nr1);
	//itoa(indexNumbers[3], nr1, 10);
	//strcat(nr, nr1);
//
	//Display_WriteSecondLine(nr);
	//int code[4];
	//switch (key)
	//{
		//case KEY_RIGHT:
			//currentIndex++;
		//break;
		//case KEY_LEFT:
			//currentIndex--;
			//break;
		//case KEY_DOWN:
			//indexNumbers[currentIndex] --;
			//if (indexNumbers[currentIndex] < 0)
				//indexNumbers[currentIndex] = 0;
			//break;
		//case KEY_UP: 
			//indexNumbers[currentIndex] ++;
			//if (indexNumbers[currentIndex] > 3)
				//indexNumbers[currentIndex] = 3;
			//break;
		//case KEY_OK:
			//
			////FlashDrive_WriteToFlashDrive(PASSCODE_FLASH_ID, indexNumbers, sizeof(int) * PASSCODE_SIZE);
			//FlashDrive_ReadFromFlashDrive(PASSCODE_FLASH_ID, code, sizeof(int) * PASSCODE_SIZE);
			//int i = 0;
			//int match = 1;
			//for (; i < PASSCODE_SIZE; i++)
			//{
				//if (code[i] != indexNumbers[i])
				//{
					//match = 0;
					//break;
				//}
			//}
			//
			//if (match == 1)
			//{
				//Lockscreen_Disable();
			//}
			//break;
		//default:
		//break;
	//}
	//
	//if (currentIndex > 3)
		//currentIndex = 3;
	//else if (currentIndex < 0)
		//currentIndex = 0;
}

// Enables the lockscreen.
void Lockscreen_Enable(void)
{

}

// Disables the lockscreen.
void Lockscreen_Disable(void)
{

}

// Sets lockscreen passcode.
// Returns 1 on success and -1 on error.
int8_t Lockscreen_SetPasscode(int code[PASSCODE_SIZE])
{
	int codeToSave = CodeArrayToCode(code, PASSCODE_SIZE);
	int result = FlashDrive_WriteToFlashDrive(PASSCODE_FLASH_ID, &codeToSave, sizeof(int));
	
	return (result == -1) ? -1 : 1; 
}

// Compares lockscreen passcode with entered code.
// Returns 0 if not, 1 if true and -1 on error.
int8_t Lockscreen_IsPasscodeValid(int code[PASSCODE_SIZE])
{
	// Encrypt code and compare it with the encrypted code on the flash drive.
	
	const int _code = CodeArrayToCode(code, PASSCODE_SIZE);
	int storedCode = 0;
	int result = FlashDrive_ReadFromFlashDrive(PASSCODE_FLASH_ID, &storedCode, sizeof(int));
	if (result == -1)
		return -1;
	
	if (_code == storedCode)
		return 1;
	
	return 0;
}

// Returns 0 if disabled and 1 on enabled. Deprecated
int Lockscreen_IsLockscreenEnabled(void)
{
	return -1;
}

// Returns whether or not the lockscreen is authenticated.
// 1 if yes, 0 if no.
int8_t Lockscreen_Authenticated(void)
{
	return authenticated;
}