/*
 * synthesizer.c
 *
 * Created: 21-3-2019 10:45:26
 *  Author: InfraRoderik
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <sys/timer.h>
#include <string.h>
#include <stddef.h>
#include "hardware/display.h"
#include "control/flash_drive.h"
#include "application/synthesizer.h"

void synthesizer_handleKeys(int key);
void synthesizer_handleRecord(int tune);
void synthesizer_saveLists(void);

/* Defining the keys for when they are pressed */
#define CLICK_KEY_1 1
#define CLICK_KEY_2 2
#define CLICK_KEY_3 3
#define CLICK_KEY_4 4
#define CLICK_KEY_5 5
#define CLICK_KEY_6 6
#define CLICK_KEY_ESC 7
#define CLICK_KEY_UP 8
#define CLICK_KEY_OK 9
#define CLICK_KEY_LEFT 10
#define CLICK_KEY_DOWN 11
#define CLICK_KEY_RIGHT 12

int recordStarted = 0;
int tunes[50];
int delays[20];
int CurrentIdx = 1;

//time_t lastTime = time(NULL);
//time_t currTime = time(NULL);

void synthesizer_handleRecord(int tune){
	if(recordStarted == 1){
		tunes[CurrentIdx] = tune;
		//currTime = time(NULL);
		//delays[CurrentIdx] = currTime - lastTime;
		CurrentIdx ++;
		//lastTime = currTime;
	}
	if(CurrentIdx > 49){
		recordStarted = 0;
		Display_Clear();
		Display_WriteFirstLine("   SYNTHESIZER  ");
		synthesizer_saveLists();
		CurrentIdx = 1;
	}
}

void synthesizer_saveLists(){
	tunes[0] = CurrentIdx;
	FlashDrive_WriteToFlashDrive(NOTES_FLASH_ID, tunes, sizeof(tunes));
	//FlashDrive_WriteToFlashDrive(DELAY_FLASH_ID, &delays, sizeof(delays));
}

void synthesizer_handleKeys(int key){

	switch (key) {
		
		/* Ok */
		case CLICK_KEY_OK:
		if(recordStarted == 0){
			recordStarted = 1;
			Display_Clear();
			Display_WriteFirstLine("   SYNTHESIZER r");
			//lastTime = time(NULL);
			}
			else{
				recordStarted = 0;
				Display_Clear();
				Display_WriteFirstLine("   SYNTHESIZER  ");
				synthesizer_saveLists();
				CurrentIdx = 1;
			}
			
		break;
		
		/* Up */
		case CLICK_KEY_UP:
		break;
		
		/* Down */
		case CLICK_KEY_DOWN:
		VsBeep(66, 2000);
		synthesizer_handleRecord(66);
		break;
		
		/* Left */
		case CLICK_KEY_LEFT:
		break;
		
		/* Right */
		case CLICK_KEY_RIGHT:
		break;
		
		case CLICK_KEY_1:
		VsBeep(162, 2000);
		synthesizer_handleRecord(162);
		break;
		
		case CLICK_KEY_2:
		VsBeep(227, 2000);
		synthesizer_handleRecord(227);
		break;
		
		case CLICK_KEY_3:
		VsBeep(1, 2000);
		synthesizer_handleRecord(1);
		break;
		
		case CLICK_KEY_4:
		VsBeep(196, 2000);
		synthesizer_handleRecord(196);
		break;
		
		case CLICK_KEY_5:
		VsBeep(33, 2000);
		synthesizer_handleRecord(33);
		break;
		
		case CLICK_KEY_6:
		VsBeep(197, 2000);
		synthesizer_handleRecord(197);
		break;
		
		default:
		break;
}
}

