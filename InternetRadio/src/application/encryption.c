/*
 * encryption.c
 *
 * Created: 22-3-2019 11:07:36
 *  Author: Dion van der Linden
 */ 
#include "application/encryption.h"
#include "util/aes.h"

#include <string.h>
#include <stdlib.h>

void GetRandomIV(uint8_t iv[IV_SIZE]);
int GetRandomHexValue(void);

static uint8_t key[AES256] =
{
	0xf0, 0x4b, 0x1b, 0xe2, 0xa0, 0xa4, 0x22, 0x37, 0xac, 0x91, 0xd3, 0x30, 0x7b, 0x1a, 0x01, 0x0e,
	0xda, 0xb0, 0x8f, 0xa2, 0xc6, 0x23, 0x48, 0x87, 0xca, 0x13, 0xae, 0x24, 0x1d, 0xf5, 0xbb, 0xef
};

BOOL Encryption_Init(void)
{
	srand(time(NULL));
	return B_TRUE;
}
BOOL Encryption_Decrypt(const uint8_t encryptedData[], const uint8_t iv[IV_SIZE], uint8_t* decryptedData)
{
	struct AES_ctx ctx;
	AES_init_ctx_iv(&ctx, key, iv);
	AES_CTR_xcrypt_buffer(&ctx, decryptedData, PASSCODE_SIZE);

	
	return B_TRUE;
}
BOOL Encryption_Encrypt(const char* data, uint8_t* encryptedData, uint8_t* ivOut)
{
	//uint8_t key[] = "secret key";

	uint8_t* in = (uint8_t*)data;
	
	uint8_t iv[IV_SIZE];
	GetRandomIV(iv);
	
	printf("Length: %zu\n", strlen((char*)in));
	
	struct AES_ctx ctx;
	AES_init_ctx_iv(&ctx, key, iv);
	AES_CTR_xcrypt_buffer(&ctx, in, strlen((char*)in));
	
	ivOut = iv;
	encryptedData = in;
	
	return B_TRUE;
}

int EncryptCBC(void)
{
	//uint8_t key[] = { 0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c };
	uint8_t out[] = { 0x76, 0x49, 0xab, 0xac, 0x81, 0x19, 0xb2, 0x46, 0xce, 0xe9, 0x8e, 0x9b, 0x12, 0xe9, 0x19, 0x7d,
		0x50, 0x86, 0xcb, 0x9b, 0x50, 0x72, 0x19, 0xee, 0x95, 0xdb, 0x11, 0x3a, 0x91, 0x76, 0x78, 0xb2,
		0x73, 0xbe, 0xd6, 0xb8, 0xe3, 0xc1, 0x74, 0x3b, 0x71, 0x16, 0xe6, 0x9e, 0x22, 0x22, 0x95, 0x16,
	0x3f, 0xf1, 0xca, 0xa1, 0x68, 0x1f, 0xac, 0x09, 0x12, 0x0e, 0xca, 0x30, 0x75, 0x86, 0xe1, 0xa7 };
	
	uint8_t iv[]  = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };
	uint8_t in[]  = { 0x6b, 0xc1, 0xbe, 0xe2, 0x2e, 0x40, 0x9f, 0x96, 0xe9, 0x3d, 0x7e, 0x11, 0x73, 0x93, 0x17, 0x2a,
		0xae, 0x2d, 0x8a, 0x57, 0x1e, 0x03, 0xac, 0x9c, 0x9e, 0xb7, 0x6f, 0xac, 0x45, 0xaf, 0x8e, 0x51,
		0x30, 0xc8, 0x1c, 0x46, 0xa3, 0x5c, 0xe4, 0x11, 0xe5, 0xfb, 0xc1, 0x19, 0x1a, 0x0a, 0x52, 0xef,
	0xf6, 0x9f, 0x24, 0x45, 0xdf, 0x4f, 0x9b, 0x17, 0xad, 0x2b, 0x41, 0x7b, 0xe6, 0x6c, 0x37, 0x10 };
	
	struct AES_ctx ctx;
	AES_init_ctx_iv(&ctx, key, iv);
	AES_CBC_encrypt_buffer(&ctx, in, 64);

	printf("CBC encrypt: ");

	if (0 == memcmp((char*) out, (char*) in, 64)) {
		printf("SUCCESS!\n");
		return(0);
		} else {
		printf("FAILURE!\n");
		return(1);
	}
}

// Returns a random hex value.
int GetRandomHexValue(void)
{
	// uint8_t max value.
	const int MAX_VALUE = 256;
	
	// Returns a random hex between 0x00 and 0xff.
	int randomHex = (rand() % MAX_VALUE) & 0xff;
	randomHex |= ((rand() % MAX_VALUE) & 0xff) << 8;
	randomHex |= ((rand() % MAX_VALUE) & 0xff) << 16;
	randomHex |= ((rand() % MAX_VALUE) & 0xff) << 24;
	
	return randomHex;
}

// Returns a random IV uint8_t array.
void GetRandomIV(uint8_t iv[IV_SIZE])
{
	int i = 0;
	for (; i < IV_SIZE; i++)
	{
		iv[i] = GetRandomHexValue();
	}
}
