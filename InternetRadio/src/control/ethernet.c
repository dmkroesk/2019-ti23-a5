/*
 * ethernet.c
 *
 * Created: 19-3-2019 10:58:11
 *  Author: Dion van der Linden
 */ 
#include "control/ethernet.h"
#include <sys/socket.h>
//#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <netdb.h>
#include <dev/nicrtl.h>
#include <stdlib.h>
#include <arpa/inet.h> /* [LiHa] For inet_addr() */
#include <pro/sntp.h> /* [LiHa] For NTP */
#include <stdio.h>
#include <string.h>

#include "control/radio_settings.h"
#include "hardware/log.h"



#define ETH0_BASE	0xC300
#define ETH0_IRQ	5

#define OK 0
#define NOK -1

static char eth0IfName[9] = "eth0";

// Init ethernet.
int Ethernet_Init(void)
{
	uint8_t macAddr[6] = { 0x00, 0x06, 0x98, 0x30, 0x02, 0x76 };
	//unsigned char* macAddr = NULL;
	//RadioSettings_GetMacAddress(macAddr);
	
	int result = OK;

	// Registreer NIC device (located in nictrl.h)
	result = NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ);

	printf("NutRegisterDevice() returned %d\n", result);
	if(result)
	{
		printf("Error: >> NutRegisterDevice()\n");
		result = NOK;
	}
	
	if( OK == result )
	{
		printf("Getting IP address (DHCP)\n");
		if( NutDhcpIfConfig(eth0IfName, NULL, 3000) )
		{
			printf("Error: no stored MAC address, try flash MAC\n");
			if( NutDhcpIfConfig(eth0IfName, macAddr, 3000) )
			{
				printf("Error: >> NutDhcpIfConfig()\n");
			
				result = NOK;
			}
		}
	}
	
	if( OK == result )
	{
		printf("Networking setup OK, new settings are:\n");

		printf("if_name: %s\n", confnet.cd_name);
		printf("ip-addr: %s\n", inet_ntoa(confnet.cdn_ip_addr));
		printf("ip-mask: %s\n", inet_ntoa(confnet.cdn_ip_mask));
		printf("gw     : %s\n", inet_ntoa(confnet.cdn_gateway));
	}
	
	return result;
}

// Syncs the time with an NTP server.
// Supply a null {0} initialized tm struct.
int Ethernet_NTPSync(struct _tm *ntpDateTime)
{
	time_t ntpTime;
	uint32_t timeServer = 0;
	tm* dt = {0};
	
	timeServer = inet_addr("95.46.198.21");			//("91.148.192.49"); 
	if (NutSNTPGetTime(&timeServer, &ntpTime) != 0) 
	{
		printf("Failed to retrieve time\n");
		return NOK;
	} 
	else 
	{
		dt = localtime(&ntpTime);
		dt->tm_hour = dt->tm_hour + 6;
		dt->tm_mon = dt->tm_mon + 1;
		dt->tm_year = 1900 + dt->tm_year;
		
		mktime(ntpDateTime);
		
		memcpy(ntpDateTime, dt, sizeof(tm));

		return OK;
	}
}

u_long Ethernet_DnsGetHostByName(const u_char* name)
{
	return NutDnsGetHostByName(name);
}
