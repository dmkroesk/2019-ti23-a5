/*
* radio_alarm.c
*
* Created: 12-3-2019 10:48:29
*  Author: Max van Noordennen
*/

#include "control/radio_alarm.h"
#include "control/radio_settings.h"
#include "control/flash_drive.h"
#include "hardware/vs10xx.h"
#include "application/menu.h"
#include <string.h>
#include <stdlib.h>
#include <sys/thread.h>

#define ALARM_ON 1
#define ALARM_OFF 0
#define STANDARD_ALARM_SIZE 5
#define WEEK_ALARM_SIZE 7

int radio_alarmAlarmOn = ALARM_OFF;
struct _alarm Alarm_alarm[STANDARD_ALARM_SIZE];
tm gmt;

void radio_alarmInit(void);
int dayofweek(int d, int m, int y);
void SetAlarmActive(void);

void Alarm_CheckAlarm(){
	//Sets the gmt struct to the correct time from the rtc
	RadioSettings_GetDateTime(&gmt);
	gmt.tm_year = gmt.tm_year + 1900;
	gmt.tm_mon = gmt.tm_mon + 1;
	
	//Create for loop to check the 5 standard alarms in Alar_alarm
	int i;
	for (i = 0; i < STANDARD_ALARM_SIZE; i++)
	{
		//Check if the alarm is on
		if (Alarm_alarm[i].on == ALARM_ON)
		{
			//Check if the alarm time is reached
			if (gmt.tm_hour == Alarm_alarm[i].hour && gmt.tm_min == Alarm_alarm[i].minute && gmt.tm_sec == 0)
			{
				SetAlarmActive();
			}
		}
	}
	
}

void SetAlarmActive(){
	//radio_alarmAlarmOn makes the alarmThread active
	radio_alarmAlarmOn = ALARM_ON;
	//Notify the menu
	Menu_AlarmGoesOf();
}

int Alarm_SaveAlarm(void)
{
	return FlashDrive_WriteToFlashDrive(ALARM_FLASH_ID, &Alarm_alarm, sizeof(_alarm) * 5);
	//FlashDrive_WriteToFlashDrive(ALARM_FLASH_ID, &alArr, sizeof(_alarm) * 5);
}
int Alarm_LoadAlarm(void)
{
	_alarm* alarms = (_alarm*)malloc (sizeof(_alarm) * 5);
	int returnCode = FlashDrive_ReadFromFlashDrive(ALARM_FLASH_ID, alarms, sizeof(_alarm) * 5);
	
	if (returnCode == 0)
	{
		memcpy(Alarm_alarm, alarms, sizeof(_alarm)*5);
	}
	else
	{
		_alarm defAlarms[] = {
			{
				12,
				10,
				"Alarm01",
				0
			},
			{	12,
				10,
				"Alarm02",
				0
			},
			{
				12,
				10,
				"Alarm03",
				0
			},
			{
				12,
				10,
				"Alarm04",
				0
			},
			{
				12,
				10,
				"Alarm05",
				0
			}
		};
		memcpy(Alarm_alarm, defAlarms, sizeof(_alarm)*5);
	}
	
	free(alarms);
	
	return returnCode;
}

void Alarm_ResetToDefault(void)
{
	_alarm* alarms = (_alarm*)malloc (sizeof(_alarm) * 5);
	
	_alarm defAlarms[] = {
		{
			12,
			10,
			"Alarm01",
			0
		},
		{	12,
			10,
			"Alarm02",
			0
		},
		{
			12,
			10,
			"Alarm03",
			0
		},
		{
			12,
			10,
			"Alarm04",
			0
		},
		{
			12,
			10,
			"Alarm05",
			0
		}
	};
	memcpy(Alarm_alarm, defAlarms, sizeof(_alarm)*5);
	
	free(alarms);
	
	Alarm_SaveAlarm();
}



void radio_alarmInit(){
	RadioSettings_GetDateTime(&gmt);
	
	gmt.tm_year = gmt.tm_year + 1900;
	gmt.tm_mon = gmt.tm_mon + 1;
}




