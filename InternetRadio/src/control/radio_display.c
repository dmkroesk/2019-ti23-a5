/*
 * radio_display.c
 *
 * Created: 21-2-2019 09:36:12
 *  Author: Dion van der Linden & Max van Noordennen
 */ 
#include "control/radio_display.h"
#include <string.h>
#include <stdlib.h>
#include "hardware/display.h"

#define MAX_CHARS_LINE 39
#define MAX_CHARS_TOTAL 78
#define MAX_VISIBLE_CHARS_LINE 16
#define MAX_VISBLE_CHARS_TOTAL 32
static int cursorPosition;

// Initialiseert de display.
void Display_Init(void)
{
	cursorPosition = 0;
	LcdLowLevelInit();
}

// Togglet de backlight, 0 = uit 1 = aan.
void Display_ToggleBacklight(int status)
{
	if (status == 0)
		LcdBackLight(0);
	else if (status == 1)
		LcdBackLight(1);
}

void Display_Write(const char* str)
{
	const int length = strlen(str);
	if (length > MAX_VISBLE_CHARS_TOTAL)
		return;
		
	//int remainingIndexes = MAX_CHARS_TOTAL - length;
	const int lines = (length / MAX_VISIBLE_CHARS_LINE) + 1;
	int remainingIndexes = (lines == 1) ? MAX_VISIBLE_CHARS_LINE - length : MAX_VISBLE_CHARS_TOTAL - length;

	const int totalStrSize = length + remainingIndexes;
	char completeStr[totalStrSize];
	//char* completeStr = malloc(length + remainingIndexes);
	strcpy(completeStr, str);
	int i;
	for (i = length; i < totalStrSize; i++)
	{
		completeStr[i] = ' ';
	}
	completeStr[totalStrSize] = '\0';
	
	cursorPosition += length + remainingIndexes;
	LcdWriteString(completeStr);
}
void Display_Clear(void)
{
	cursorPosition = 0;
	LcdClearDisplay();
}

void Display_WriteFirstLine(char* string)
{
	int i = 0;
	for(i = 0; string[i] != '\0'; ++i)
	{
		LcdChar(string[i]);
	}
}



void Display_WriteSecondLine(char* string)
{
	LcdWriteCommand(0x40 | 0x80);
	
	int i = 0;
	for(i = 0; string[i] != '\0'; ++i)
	{
		LcdChar(string[i]);
	}
}

