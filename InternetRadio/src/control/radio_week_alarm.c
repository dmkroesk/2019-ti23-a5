/*
 * radio_week_alarm.c
 *
 * Created: 22-3-2019 10:06:44
 *  Author: timde
 */ 
#include "control/radio_alarm.h"
#include "control/radio_settings.h"
#include "control/flash_drive.h"
#include "application/menu.h"
#include <string.h>
#include <stdlib.h>

//Defines for the week alarms
#define ALARM_ON 1
#define ALARM_OFF 0
#define STANDARD_ALARM_SIZE 5
#define WEEK_ALARM_SIZE 7

//Variables for the week alarms
struct _alarm Week_alarms[WEEK_ALARM_SIZE];
tm gmt;

//Prototypes for the week alarms
void radio_week_alarm_SaveWeekAlarms(void);
void SetWeekAlarmActive(void);
int dayofweek(int Day, int Month, int Year);

/*
Function is called from main and checks if an alarm needs to go of
*/
void radio_week_alarm_CheckAlarm(){
	//WeekIndex: The day of the week (0...6) where 0 is sunday.
	int WeekIndex = dayofweek(gmt.tm_mday, gmt.tm_mon, gmt.tm_year);
	//Select the current weekday alarm in the array and check if it is on
	if(Week_alarms[WeekIndex].on == ALARM_ON){
		//Check if the alarm time is reached
		if(gmt.tm_hour == Week_alarms[WeekIndex].hour && gmt.tm_min == Week_alarms[WeekIndex].minute && gmt.tm_sec == 0){
			SetWeekAlarmActive();
		}
	}
}

/*
Function is called when the alarm goes active
*/
void SetWeekAlarmActive(){
	//radio_alarmAlarmOn makes the alarmThread active
	radio_alarmAlarmOn = ALARM_ON;
	//Notify the menu
	Menu_AlarmGoesOf();
}

/*
Day: The day of the month of the date
Month: The month of the year of the date
Year: The year of the date
Offset: An array that holds the offset for each month
Weekday: The variable which is altered to get the day of the week from the parameter values
return: the weekday of the date, 0...6 where 0 = SUNDAY
*/
int dayofweek(int Day, int Month, int Year)
{
	static int Offset[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
	//Puts an extra day in the year
	Year -= Month < 3;
	//Adds a day for every year and leapYear that has elapsed
	int WeekDay = Year + Year/4;
	//Substracts a day for every leapyear that is skipped every 100 years
	//Adds a day for every leapyear on every 400th year
	WeekDay = WeekDay - Year/100 + Year/400;
	//Adds the day of the month and the month offset from the array
	WeekDay = WeekDay + Day + Offset[Month-1];
	//Finally modulo 7 (week length)the total amount to get a value between 0 and 6
	return WeekDay = WeekDay % 7;
}

/*
Writes the Week_alarms to the flash on "WEEKALARM_FLASH_ID"(130) address
*/
void radio_week_alarm_SaveWeekAlarms(){
	FlashDrive_WriteToFlashDrive(WEEKALARM_FLASH_ID, &Week_alarms, sizeof(_alarm) * WEEK_ALARM_SIZE);
}

/*
WeekAlarms:			The variable that stores the alarms received from the flash
TempWeekAlarms:		An array with standard week alarms
ReturnCode:			A check value for reading from the flash
*/
void radio_week_alarm_LoadWeekAlarms(){
	//Reading from flash
	_alarm* WeekAlarmsFlash = (_alarm*)malloc (sizeof(_alarm) * 7);
	int returnCode = FlashDrive_ReadFromFlashDrive(WEEKALARM_FLASH_ID, WeekAlarmsFlash, sizeof(_alarm) * WEEK_ALARM_SIZE);
	
	//Checking if the alarms could be loaded from the flash
	if (returnCode == 0)
	{
		//Setting Week_alarms to the received alarms from the flash
		memcpy(Week_alarms, WeekAlarmsFlash, sizeof(_alarm)*WEEK_ALARM_SIZE);
	}
	else{
		//Initializing the TempWeekAlarms array
		_alarm TempWeekAlarms[WEEK_ALARM_SIZE] = {
			{
				00,
				00,
				"ZOND",
				0
			},
			{
				00,
				00,
				"MAAN",
				0
			},
			{	00,
				00,
				"DINS",
				0
			},
			{
				00,
				00,
				"WOEN",
				0
			},
			{
				00,
				00,
				"DOND",
				0
			},
			{
				00,
				00,
				"VRIJ",
				0
			},
			{
				00,
				00,
				"ZATE",
				0
			}
			
		};
		//Setting Week_alarms to standard values and saving them to the flash
		memcpy(Week_alarms, TempWeekAlarms, sizeof(_alarm)*WEEK_ALARM_SIZE);
		radio_week_alarm_SaveWeekAlarms();
	}
	free(WeekAlarmsFlash);
}