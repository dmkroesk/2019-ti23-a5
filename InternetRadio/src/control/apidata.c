/*
 * apidata.c
 *
 * Created: 20-3-2019 22:54:56
 *  Author: Max van Noordennen
 */ 


#define BUFFER_SIZE 2048
#define AUTHOR_LENGTH 9
#define AUTHOR_JSON_LENGTH 30
#define TITLE_LENGTH 8
#define TITLE_JSON_LENGTH 100
#define PDATE_LENGTH 14
#define PDATE_JSON_LENGTH 30
 
#include <sys/socket.h>
#include <sys/confnet.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/bankmem.h>
#include <sys/heap.h>

#include <arpa/inet.h>
#include <dev/nicrtl.h>
#include <pro/dhcp.h>
#include <net/route.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "control/apidata.h"

TCPSOCKET *sock;
FILE *stream;

char buff[BUFFER_SIZE];
_newsData newsData;

/*
    Function to connect to api.
*/

int ApiData_GetApiData() {  
    int timeOutValue = 15000;
	int segmentation = 1460;
	int returnBuffer = 8192;

	/*
		Initializing the socket and connecting to the server
	*/
	sock = NutTcpCreateSocket();
	NutTcpSetSockOpt(sock, 0x02, &segmentation, sizeof(segmentation));
	
	// Sends the timeout value to the tcp socket
    NutTcpSetSockOpt(sock, SO_RCVTIMEO, &timeOutValue, sizeof(timeOutValue));
	
	NutTcpSetSockOpt(sock, SO_RCVBUF, &returnBuffer, sizeof(returnBuffer));
	
   	NutTcpConnect(sock, inet_addr("167.114.118.40"), 80);

	stream = _fdopen((int) sock, "r+b");
	
	char country[2000];
	
	strcat(country,"/v2/top-headlines?country=");
	strcat(country,newsData.country);
	strcat(country,"&pageSize=1&apiKey=0e24f9d5a6bb4f0aa7a006afae91cfda");
	
	/*
		GET request to the server
	*/
	fprintf(stream, "GET %s HTTP/1.1\r\n", country);
    fprintf(stream, "Host: %s\r\n", "newsapi.org");
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
    fprintf(stream, "Connection: close\r\n\r\n");
    fflush(stream);
 
    /*
        Read server response.
    */
    while (fgets(buff, sizeof(buff), stream)) {
        puts(buff);
    }
	
	/* 
		Parsing the data 
	*/
	
	// parsing the data to search for author. Adds the author to the newsData struct
	Apidata_SearchAuthor();
	//parsing the data to search for title. Adds the title to the newsData struct
	Apidata_SearchTitle();
	// parsing the data to search for published date. Adds the published data to the newsData struct
	Apidata_SearchPublishedDate();
	
	printf("%s \n", newsData.author);
	printf("%s \n", newsData.title);
	printf("%s \n", newsData.pdate);
	
    /* 
		closing stream and socket 
	*/
	country[0] = '\0';
	
    fclose(stream);
    NutTcpCloseSocket(sock);
    return 1;
}

/*
	Function for searching and parsing the author in the buffer.
*/
void Apidata_SearchAuthor(void)
{
	// a char author with the length of AUTHOR_LENGTH
	char author[AUTHOR_LENGTH]= "author";
	// a char pointer for storing the searched data
	char* data;
	// a char for storing the parsed json
	char json[AUTHOR_JSON_LENGTH];
	
	//strstr is for searching a particular char in a char pointer
	data = strstr(buff,author);
	
	// int for iterating
	int i = 9;
	
	//while loop for parsing the json data
	while (data[i] != '"'){
		json[i-AUTHOR_LENGTH] = data[i];
		i++;
	}
	
	// at the end of the json, 
	// add a \0 to the char
	json[i-AUTHOR_LENGTH] = '\0';
	
	// strcpy for copying the char into the newsData.author
	strcpy(newsData.author,json);
}

/*
	Function for searching and parsing the title in the buffer.
*/
void Apidata_SearchTitle(void){
	// a char title with the length of TITLE_LENGTH
	char title[TITLE_LENGTH]= "title";
	// a char pointer for storing the searched data
	char* data;
	// a char for storing the parsed json
	char json[TITLE_JSON_LENGTH];
	
	//strstr is for searching a particular char in a char pointer
	data = strstr(buff,title);
	
	// int for iterating
	int i = 8;
	
	//while loop for parsing the json data
	while (data[i] != '"')
	{
		json[i-TITLE_LENGTH] = data[i];
		i++;
	}
	
	// at the end of the json,
	// add a \0 to the char
	json[i-TITLE_LENGTH] = '\0';
	
	// strcpy for copying the char into the newsData.title
	strcpy(newsData.title,json);
}

/*
	Function for searching and parsing the publishedDate in the buffer.
*/
void Apidata_SearchPublishedDate(void){
	// a char pdate with the length of PDATE_LENGTH
	char pdate[PDATE_LENGTH] = "publishedAt";
	// a char pointer for storing the searched data
	char* data;
	// a char for storing the parsed json
	char json[PDATE_JSON_LENGTH];
	
	//strstr is for searching a particular char in a char pointer
	data = strstr(buff,pdate);
	
	// int for iterating
	int i = 14;
	
	//while loop for parsing the json data
	while(data[i] != '"')
	{
		json[i-PDATE_LENGTH] = data[i];
		i++;
	}
	
	// at the end of the json,
	// add a \0 to the char
	json[i - PDATE_LENGTH] = '\0';
	
	// strcpy for copying the char into the newsData.pdate
	strcpy(newsData.pdate,json);
}
