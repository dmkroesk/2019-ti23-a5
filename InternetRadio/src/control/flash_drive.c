/*
 * flash_drive.c
 *
 * Created: 12-3-2019 16:57:31
 *  Author: Dion van der Linden
 */ 

#include "control/flash_drive.h"
#include "hardware/flash.h"

// Init the flash drive.
int FlashDrive_Init(void)
{
	return At45dbInit();
}
int FlashDrive_WriteToFlashDrive(u_long id, const void* data, u_int len)
{
	return At45dbPageWrite(id, data, len);
}
int FlashDrive_ReadFromFlashDrive(u_long id, void* data, u_int len)
{
	return At45dbPageRead(id, data, len);
}
