/*
 * player.c
 *
 * Created: 28-2-2019 10:41:04
 *  Author: Dion van der Linden
 */ 

#define LOG_MODULE LOG_PLAYER_MODULE

#include <sys/heap.h>
#include <sys/bankmem.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include "control/player.h"

#include "hardware/vs10xx.h"
#include "hardware/log.h"

#define OK	1
#define NOK	0



THREAD(StreamPlayer, arg);
static volatile int runStream;

int Player_Init()
{
	runStream = 0;

	return OK;
}

int Player_Play(FILE* stream)
{
	if (runStream == 1)
		return ALREADY_PLAYING;
	LogMsg_P(LOG_DEBUG, PSTR("Play thread created\n"));

	
	runStream = 1;
	NutThreadCreate("RadioStream", StreamPlayer, stream, 512);

	return OK;
}

int Player_Stop(FILE* stream)
{
	if (runStream == 0)
		return NO_STREAM_PLAYING;
	//VsPlayerStop();
	runStream = 0;
	//NutSleep(500);
	//fclose(stream);
	return OK;
}

THREAD(StreamPlayer, arg)
{
	FILE *stream = (FILE *) arg;
	size_t rbytes = 0;
	char *mp3buf;
	int result = NOK;
	int nrBytesRead = 0;
	unsigned char iflag;
	
	//
	// Init MP3 buffer. NutSegBuf is een globale, systeem buffer
	//
	if( 0 != NutSegBufInit(8192) )
	{
		// Reset global buffer
		iflag = VsPlayerInterrupts(0);
		NutSegBufReset();
		VsPlayerInterrupts(iflag);
		
		result = OK;
	}
	
	// Init the Vs1003b hardware
	if( OK == result )
	{
		if( -1 == VsPlayerInit() )
		{
			if( -1 == VsPlayerReset(0) )
			{
				result = NOK;
			}
		}

	}
	
	while (1)
	{
		if (runStream == 0)
		{
			printf("Stopping radio\n");
			VsPlayerStop();
			
			fclose(stream);
			NutThreadExit();
			return;
		}
		/*
		 * Query number of byte available in MP3 buffer.
		 */
        iflag = VsPlayerInterrupts(0);
        mp3buf = NutSegBufWriteRequest(&rbytes);
        VsPlayerInterrupts(iflag);
		
		// Bij de eerste keer: als player niet draait maak player wakker (kickit)
		if( VS_STATUS_RUNNING != VsGetStatus() )
		{
			if( rbytes < 1024 )
			{
				printf("VsPlayerKick()\n");
				VsPlayerKick();
			}
		}
		
		while( rbytes && runStream == 1)
		{
			// Copy rbytes (van 1 byte) van stream naar mp3buf.
			nrBytesRead = fread(mp3buf,1,rbytes,stream);
			
			if( nrBytesRead > 0 )
			{
				iflag = VsPlayerInterrupts(0);
				mp3buf = NutSegBufWriteCommit(nrBytesRead);
				VsPlayerInterrupts(iflag);
				if( nrBytesRead < rbytes && nrBytesRead < 512 )
				{
					NutSleep(250);
				}
			}
			else
			{
				break;
			}
			rbytes -= nrBytesRead;
			
			if( nrBytesRead <= 0 )
			{
				break;
			}				
		}
	}
}
