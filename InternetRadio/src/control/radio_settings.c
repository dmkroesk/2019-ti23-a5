/*
 * radio_settings.c
 *
 * Created: 19-2-2019 15:14:14
 *  Author: Dion van der Linden
 */ 

#include "control/radio_settings.h"
#include "control/flash_drive.h"
#include <string.h>
#include <stdio.h>
#include "hardware/vs10xx.h"

#define MAC_ADDRESS_SIZE 12
// Initialiseert de RadioTijd.
int RadioSettings_Init(void)
{
	return X12Init() == 0 ? 1 : 0;
}

// Zet de datum op de Radio.
// @param	int		year	= 2019
// @param	int		month	= _MONTHS_
// @param	int		day		= 1-31
// @param	int		wday	= _WEEK_DAYS_
// @param	int		hour	= 0-23
// @param	int		minute	= 0-59
// @param	int		second	= 0-59
// @return	BOOL	Geeft aan of het zetten van de tijd gelukt is.
int RadioSettings_SetDateTime(int year, int month, int day, int wday,
int hour, int minute, int second)
{
	tm setTime = {0};
	
	setTime.tm_year = year - 1900;
	setTime.tm_mon = month - 1;
	setTime.tm_wday = wday;
	setTime.tm_mday = day;
	setTime.tm_hour = hour;
	setTime.tm_min = minute;
	setTime.tm_sec = second;

	return X12RtcSetClock(&setTime) == 0 ? 1 : 0;
}

// Verkrijgt de datum en tijd van de radio.
// @param	struct_tm	tm	Tijd structure
// @return	BOOL Geeft aan of het verkrijgen van de tijd gelukt is.
int RadioSettings_GetDateTime(struct _tm *tm)
{
	return X12RtcGetClock(tm) == 0 ? 1 : 0;
}

// Verkrijgt het huidige volume van de radio.
// @return u_short Geeft het volume.
u_short RadioSettings_GetVolume(void)
{
	return VsGetVolume();
}

// Zet het volume op de radio.
// @param u_char vol Volume.
// @return int 0 Op success -1 error.
int RadioSettings_SetVolume(u_char vol)
{
	return VsSetVolume(vol, vol);
}

int RadioSettings_GetMacAddress(unsigned char* mac)
{	
	return FlashDrive_ReadFromFlashDrive(MAC_FLASH_ID, mac, sizeof(unsigned char) * MAC_ADDRESS_SIZE);
}

int RadioSettings_SaveMacAddress(const unsigned char* mac)
{
	return FlashDrive_WriteToFlashDrive(MAC_FLASH_ID, mac, sizeof(unsigned char) * MAC_ADDRESS_SIZE);
}