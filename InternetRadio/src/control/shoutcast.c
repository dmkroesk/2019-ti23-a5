/*
 * internet_shoutcast.c
 *
 * Created: 26-2-2019 12:45:55
 *  Author: MAX-TOP
 */ 

///
#define LOG_MODULE  LOG_SHOUTCAST_MODULE

#include <sys/thread.h>
#include <sys/timer.h>

#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <netdb.h>

#include "hardware/log.h"
#include "control/shoutcast.h"
#include "control/player.h"

#include <sys/socket.h>

#define ETH0_BASE	0xC300
#define ETH0_IRQ	5

#define OK			0
#define NOK			-1

#define RADIO_IPADDRESS	"162.252.85.85"   // wu world radio
#define RADIO_PORT		8872			// wu world radio
#define RADIO_URL		"/"				// wu world radio


FILE *stream;

/*
 * Init for ethernet
*/
int Shoutcast_Init(void)
{
	//uint8_t mac_addr[6] = { 0x00, 0x06, 0x98, 0x30, 0x02, 0x76 };
	//
	//int result = OK;
//
	//// Registreer NIC device (located in nictrl.h)
	//result = NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ);
	//LogMsg_P(LOG_DEBUG, PSTR("NutRegisterDevice() returned %d"), result);
	//if(result)
	//{
		//LogMsg_P(LOG_ERR, PSTR("Error: >> NutRegisterDevice()"));
		//result = NOK;
	//}
	//
	//if( OK == result )
	//{
		//LogMsg_P(LOG_INFO, PSTR("Getting IP address (DHCP)"));
		//if( NutDhcpIfConfig(eth0IfName, NULL, 3000) )
		//{
			//LogMsg_P(LOG_ERR, PSTR("Error: no stored MAC address, try hardcoded MAC"));
			//if( NutDhcpIfConfig(eth0IfName, mac_addr, 0) )
			//{
				//LogMsg_P(LOG_ERR, PSTR("Error: >> NutDhcpIfConfig()"));
				//result = NOK;
			//}
		//}
	//}
	//
	//if( OK == result )
	//{
		//LogMsg_P(LOG_INFO, PSTR("Networking setup OK, new settings are:\n") );
//
		//LogMsg_P(LOG_INFO, PSTR("if_name: %s"), confnet.cd_name);
		//LogMsg_P(LOG_INFO, PSTR("ip-addr: %s"), inet_ntoa(confnet.cdn_ip_addr) );
		//LogMsg_P(LOG_INFO, PSTR("ip-mask: %s"), inet_ntoa(confnet.cdn_ip_mask) );
		//LogMsg_P(LOG_INFO, PSTR("gw     : %s\n"), inet_ntoa(confnet.cdn_gateway) );
	//}
	//
	//return result;
	return OK;
}

/*
 * Function for connecting to a stream. 
*/
int Shoutcast_ConnectToStream(RadioStream radioStream)
{
	int result = OK;
	char *data;
	
	TCPSOCKET *sock;
	LogMsg_P(LOG_DEBUG, PSTR("IP: %s, PORT: %d, URL: %s"), radioStream.ipAddress, radioStream.port, radioStream.url);
	sock = NutTcpCreateSocket();
	
	u_long address = inet_addr(radioStream.ipAddress);
	if( NutTcpConnect(sock,
						address, 
						radioStream.port) )
	{
		int errorCode = NutTcpError(sock);
		LogMsg_P(LOG_ERR, PSTR("Error: >> NutTcpConnect() %d\n"), errorCode);
		return NOK;
	}
	stream = _fdopen( (int) sock, "r+b" );
	
	fprintf(stream, "GET %s HTTP/1.0\r\n", radioStream.url);
	fprintf(stream, "Host: %s\r\n", radioStream.ipAddress);
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
	fprintf(stream, "Icy-MetaData: 1\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);

	// Connection stream debug
	LogMsg_P(LOG_ERR, PSTR("%s"), stream);
	
	// Server stuurt nu HTTP header terug, catch in buffer
	data = (char *) malloc(2048 * sizeof(char));
	
	while( fgets(data, 2048, stream) )
	{
		if( 0 == *data )
			break;
	}
	LogMsg_P(LOG_ERR, PSTR("%s"), data);
	free(data);
	
	return result;
}

/*
 * Function for playing a stream.
*/
int Shoutcast_PlayStream(void)
{
	if (Player_Play(stream) == ALREADY_PLAYING)
	{
		Player_Stop(stream);
		Player_Play(stream);
	}
	
	return 0;
}

/*
 * Function to stop the stream.
*/
int Shoutcast_StopStream(void)
{
	printf("trying to stop stream");
	
	return Player_Stop(stream);
}

/*
 *	Checks the internet connection by sending a connection request to an IP address. 
 */
int Shoutcast_ConnectionCheck(void){
	
	TCPSOCKET *sock;
	sock = NutTcpCreateSocket();
	if( NutTcpConnect(	sock, inet_addr(RADIO_IPADDRESS), RADIO_PORT) )
	{
		//LogMsg_P(LOG_ERR, PSTR("Error: >> NutTcpConnect() %d\n"));
		
		return -1;
		
	}
}