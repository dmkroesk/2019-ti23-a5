/*
 * apidata.h
 *
 * Created: 20-3-2019 23:35:18
 *  Author: Max van Noordennen
 */ 


#ifndef APIDATA_H_
#define APIDATA_H_

#define AUTHOR_SIZE 20
#define TITLE_SIZE 200
#define PDATE_SIZE 30
#define COUNTRY_SIZE 5

typedef struct _newsData{
	char author[AUTHOR_SIZE];
	char title[TITLE_SIZE];
	char pdate[PDATE_SIZE];
	char country[COUNTRY_SIZE];
} _newsData;

_newsData newsData;
/*
    Function to connect to api.
*/
int ApiData_GetApiData();

/*
	Function for searching and parsing the author in the buffer.
*/
void Apidata_SearchAuthor(void);
/*
	Function for searching and parsing the title in the buffer.
*/
void Apidata_SearchTitle(void);
/*
	Function for searching and parsing the publishedDate in the buffer.
*/
void Apidata_SearchPublishedDate(void);


#endif /* APIDATA_H_ */
