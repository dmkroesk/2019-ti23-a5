/*
 * flash_drive.h
 *
 * Created: 12-3-2019 16:54:21
 *  Author: Dion van der Linden
 */ 


#ifndef FLASH_DRIVE_H_
#define FLASH_DRIVE_H_

#define MAC_FLASH_ID 60
#define VOLUME_FLASH_ID 61
#define ALARM_FLASH_ID 110
#define WEEKALARM_FLASH_ID 130
#define NOTES_FLASH_ID 150
#define DELAY_FLASH_ID 200
#define PASSCODE_FLASH_ID 160
#define KEYFILE_FLASH_ID 170

#include <sys/types.h>
int FlashDrive_Init(void);
int FlashDrive_WriteToFlashDrive(u_long id, const void* data, u_int len);
int FlashDrive_ReadFromFlashDrive(u_long id, void* data, u_int len);


#endif /* FLASH_DRIVE_H_ */
