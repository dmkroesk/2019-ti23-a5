/*
 * radio_settings.h
 *
 * Created: 19-2-2019 15:14:00
 *  Author: Dion van der Linden
 */ 


#ifndef RADIO_SETTINGS_H_
#define RADIO_SETTINGS_H_

#include <sys/types.h>
#include "hardware/rtc.h"
int RadioSettings_Init(void);
int RadioSettings_SetDateTime(int year, int month, int day, int wday, int hour, int minute, int second);
int RadioSettings_GetDateTime(struct _tm *tm);
int RadioSettings_GetMacAddress(unsigned char* mac);
int RadioSettings_SaveMacAddress(const unsigned char* mac);
u_short RadioSettings_GetVolume(void);
int RadioSettings_SetVolume(u_char vol);

#endif /* RADIO_SETTINGS_H_ */
