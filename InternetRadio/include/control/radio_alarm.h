/*
 * radio_alarm.h
 *
 * Created: 12-3-2019 10:49:55
 *  Author: Max van Noordennen
 */ 

#include "hardware/vs10xx.h"


#ifndef RADIO_ALARM_H_
#define RADIO_ALARM_H_

typedef struct _alarm{
	int hour;
	int minute;
	char alarmName[8];
	int on;
} _alarm;

extern _alarm Alarm_alarm[];

extern int radio_alarmAlarmOn;
void Alarm_CheckAlarm(void);
int Alarm_SaveAlarm(void);
int Alarm_LoadAlarm(void);
void Alarm_ResetToDefault(void);
void radio_alarmInit(void);

#endif /* RADIO_ALARM_H_ */
