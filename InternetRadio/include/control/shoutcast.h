/*
 * internet_shoutcast.h
 *
 * Created: 26-2-2019 12:56:55
 *  Author: MAX-TOP
 */ 


#ifndef INTERNET_SHOUTCAST_H_
#define INTERNET_SHOUTCAST_H_

#define MAX_IP_ADDRESS_LENGTH 20
#define MAX_URL_LENGTH 100

#include <sys/types.h>

typedef struct
{
	char ipAddress[125];
	u_short port;
	char url[100];
} RadioStream;

int Shoutcast_Init(void);
int Shoutcast_ConnectToStream(RadioStream radioStream);
int Shoutcast_PlayStream(void);
int Shoutcast_StopStream(void);
int Shoutcast_ConnectionCheck(void);

#endif /* INTERNET_SHOUTCAST_H_ */
