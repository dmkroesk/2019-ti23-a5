/*
 * ethernet.h
 *
 * Created: 19-3-2019 10:58:32
 *  Author: A5_TI2.3
 */ 
#include <sys/types.h>
#include <time.h>

#ifndef ETHERNET_H_
#define ETHERNET_H_

int Ethernet_Init(void);
int Ethernet_NTPSync(struct _tm *ntpDateTime);
u_long Ethernet_DnsGetHostByName(const u_char* name);

#endif /* ETHERNET_H_ */
