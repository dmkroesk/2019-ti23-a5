/*
 * radio_display.h
 *
 * Created: 21-2-2019 09:36:01
 *  Author: Dion van der Linden
 */ 


#ifndef RADIO_DISPLAY_H_
#define RADIO_DISPLAY_H_

void Display_Init(void);
void Display_ToggleBacklight(int status);
void Display_Write(const char* str);
void Display_Clear(void);
void Display_WriteFirstLine(char* string);
void Display_WriteSecondLine(char* string);


#endif /* RADIO_DISPLAY_H_ */
