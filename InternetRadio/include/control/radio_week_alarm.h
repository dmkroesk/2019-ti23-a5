/*
 * radio_week_alarm.h
 *
 * Created: 22-3-2019 10:11:10
 *  Author: timde
 */ 

//Global functions
void radio_week_alarm_CheckAlarm(void);
void radio_week_alarm_LoadWeekAlarms(void);
void radio_week_alarm_SaveWeekAlarms(void);

//Global variables
extern _alarm Week_alarms[];