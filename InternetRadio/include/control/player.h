/*
 * player.h
 *
 * Created: 28-2-2019 10:37:56
 *  Author: Dion van der Linden
 */ 


#ifndef PLAYER_H_
#define PLAYER_H_

#define ALREADY_PLAYING 2
#define NO_STREAM_PLAYING 3

#include <sys/nutconfig.h>
#include <sys/types.h>

#include <stdio.h>
#include <io.h>

int Player_Init(void);
int Player_Play(FILE* stream);
int Player_Stop(FILE* stream);

#endif /* PLAYER_H_ */
