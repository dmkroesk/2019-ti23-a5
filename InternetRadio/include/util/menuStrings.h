/*
 * menuStrings.h
 *
 * Created: 22-2-2019 10:27:02
 *  Author: A5
 */ 

//Date time & Welkom strings
extern char DateString[17];
extern char WelcomeString[17];
extern char ClickButtonString[17];
extern char EnterCodeString[17];

//Main Menu items
extern char MainMenuString[17];
extern char AlarmArrowString[17];
extern char AlarmString[17];
extern char SettingsArrowString[17];
extern char SettingsWifiString[17];
extern char RadioArrowString[17];
extern char RadioWifiString[17];
extern char WeekAlarmArrowString[17];
extern char WeekAlarmWifiString[17];


//Alarm items
extern char AlarmListString[17];
extern char AlarmDetailString[17];
extern char AlarmDetail2String[17];
extern char AlarmArrowString[17];

//Settings items
extern char SettingsWifiString[17];
extern char VolumeArrowString[17];
extern char VolumeWifiString[17];
extern char SnoozeArrowString[17];
extern char SnoozeWifiString[17];
extern char MacArrowString[17];
extern char MacWifiString[17];
extern char LanguageArrowString[17];
extern char SnoozeTimeString[17];
extern char LanguageWifiString[17];
extern char LanguageNlArrowString[17];
extern char LanguageNlWifiString[17];
extern char LanguageEnString[17];
extern char ChangeTimeSettingsString[17];
extern char ChangeTimeWifiString[17];
extern char ChangeNpmAutoString[17];
extern char ChangeNpmAutoWifiString[17];

//Time items
extern char SelectHoursString[17];
extern char SelectMinsString[17];

//Radio items
extern char RadioMenuString[17];
extern char RadioChannel1ArrowString[17];
extern char RadioChannel1WifiString[17];
extern char RadioChannel2ArrowString[17];
extern char RadioChannel2WifiString[17];
extern char RadioChannel3ArrowString[17];
extern char RadioChannel3WifiString[17];
extern char RadioOffArrowString[17];

//animation
extern char AnimationChar1[17];
extern char AnimationChar2[17];

