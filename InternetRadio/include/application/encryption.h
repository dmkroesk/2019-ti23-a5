/*
 * encryption.h
 *
 * Created: 22-3-2019 11:07:24
 *  Author: Dion van der Linden
 */ 


#ifndef ENCRYPTION_H_
#define ENCRYPTION_H_


#include <sys/types.h>
#include <time.h>
#include <stdio.h>

#define AES256	32
#define IV_SIZE 16
#define PASSCODE_SIZE 4


#define B_TRUE 0
#define B_FALSE !B_TRUE
typedef int BOOL;

BOOL Encryption_Init(void);
BOOL Encryption_Decrypt(const uint8_t encryptedData[], const uint8_t iv[IV_SIZE], uint8_t* decryptedData);
BOOL Encryption_Encrypt(const char* data, uint8_t* encryptedData, uint8_t* ivOut);


#endif /* ENCRYPTION_H_ */
