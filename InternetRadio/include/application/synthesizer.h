/*
 * synthesizer.h
 *
 * Created: 21-3-2019 11:52:33
 *  Author: InfraRoderik
 */ 

void synthesizer_handleKeys(int key);
void synthesizer_handleRecord(int tune);
void synthesizer_saveLists(void);