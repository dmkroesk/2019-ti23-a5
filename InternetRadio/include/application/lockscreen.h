/*
 * lockscreen.h
 *
 * Created: 21-3-2019 14:55:53
 *  Author: Dion van der Linden
 */ 

#include <sys/types.h>
#ifndef LOCKSCREEN_H_
#define LOCKSCREEN_H_

#define PASSCODE_SIZE 4

void Lockscreen_Init(void);
void Lockscreen_Update(u_char key); // Deprecated
int8_t Lockscreen_Unlock(int code[PASSCODE_SIZE]);
void Lockscreen_Enable(void); // Deprecated
void Lockscreen_Disable(void); // Deprecated
int8_t Lockscreen_SetPasscode(int code[PASSCODE_SIZE]); 
int8_t Lockscreen_IsPasscodeValid(int code[PASSCODE_SIZE]);
int8_t Lockscreen_Authenticated(void);
int Lockscreen_IsLockscreenEnabled(void); // Deprecated

#endif /* LOCKSCREEN_H_ */
